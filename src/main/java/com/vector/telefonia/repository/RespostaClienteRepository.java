package com.vector.telefonia.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.telefonia.models.RespostaCliente;

public interface RespostaClienteRepository extends JpaRepository<RespostaCliente,Long> {

}
