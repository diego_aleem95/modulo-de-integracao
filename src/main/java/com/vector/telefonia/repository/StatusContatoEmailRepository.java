package com.vector.telefonia.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.telefonia.models.StatusContatoEmail;

public interface StatusContatoEmailRepository extends JpaRepository<StatusContatoEmail, Long>{
	

}
