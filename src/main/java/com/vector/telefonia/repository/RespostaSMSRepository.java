package com.vector.telefonia.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.telefonia.models.RespostaSMS;
import com.vector.telefonia.repository.respostaSMS.RespostaSMSRepositoryQuery;

public interface RespostaSMSRepository extends JpaRepository<RespostaSMS,Long>, RespostaSMSRepositoryQuery {

	Optional<RespostaSMS> findByCorpo(Long cdCorpo);

}