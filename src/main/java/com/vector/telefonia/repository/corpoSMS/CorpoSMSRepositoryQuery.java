package com.vector.telefonia.repository.corpoSMS;

public interface CorpoSMSRepositoryQuery {
	
	Integer getPrimaroCorpoSMSPorAtivo(Long cdAtivo);
	

	Integer getProximoCorpoPerguntaSMS(Long cdAtivo, Long cdCorpo);
}
