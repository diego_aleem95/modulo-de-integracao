package com.vector.telefonia.repository.corpoSMS;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class CorpoSMSRepositoryImpl implements CorpoSMSRepositoryQuery{
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Integer getPrimaroCorpoSMSPorAtivo(Long cdAtivo) {
 		String sql = "SELECT tb_corpo_sms.cd_pergunta FROM maladireta.tb_corpo_sms WHERE tb_corpo_sms.cd_maladireta = "+cdAtivo+" ORDER BY tb_corpo_sms.cd_pergunta ASC LIMIT 1";
		Query query = manager.createNativeQuery(sql);
		Integer cdCorpo = Integer.valueOf(query.getSingleResult().toString());
		return cdCorpo;
	}

	@Override
	public Integer getProximoCorpoPerguntaSMS(Long cdAtivo, Long cdCorpo) {
		String sql = "SELECT tb_corpo_sms.cd_pergunta FROM maladireta.tb_corpo_sms WHERE tb_corpo_sms.cd_maladireta = "+cdAtivo+" AND tb_corpo_sms.cd_pergunta > "+cdCorpo+" ORDER BY tb_corpo_sms.cd_pergunta ASC LIMIT 1";
		Query query = manager.createNativeQuery(sql);
		if(query.getSingleResult().toString() != null || !query.getSingleResult().toString().isEmpty()) {
			Integer idCorpo = Integer.valueOf(query.getSingleResult().toString());
			return idCorpo;	
		}
		
		return null;
	}

	
}
