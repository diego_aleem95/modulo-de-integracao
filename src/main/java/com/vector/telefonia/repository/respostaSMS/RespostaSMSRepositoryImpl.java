package com.vector.telefonia.repository.respostaSMS;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class RespostaSMSRepositoryImpl implements RespostaSMSRepositoryQuery{
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Long getCdMalaDireta(Long cdMalaDireta) {
		String sql = "SELECT resposta.cd_resposta FROM maladireta.tb_resposta_sms AS resposta WHERE resposta.cd_corpo_sms = "+ cdMalaDireta;
		System.out.println(sql);
		Query q = manager.createNativeQuery(sql);
		Long qtd = ((Number)q.getSingleResult()).longValue();
		return  qtd;
	}

	
}
