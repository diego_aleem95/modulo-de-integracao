package com.vector.telefonia.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.telefonia.models.StatusSMS;

public interface StatusSMSRepository extends JpaRepository<StatusSMS, Long>{

}
