package com.vector.telefonia.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.telefonia.models.CorpoSMS;
import com.vector.telefonia.repository.corpoSMS.CorpoSMSRepositoryQuery;


public interface CorpoSMSRepository extends JpaRepository<CorpoSMS,Long>, CorpoSMSRepositoryQuery {

}