package com.vector.telefonia.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.telefonia.models.ContatoMalaDireta;
import com.vector.telefonia.models.Lote;



public interface ContatoEmailRepository extends JpaRepository<ContatoMalaDireta, Long>{

	List<ContatoMalaDireta> findAllByLote(Lote lote);

}
