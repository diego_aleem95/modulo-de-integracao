package com.vector.telefonia.resouces;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vector.telefonia.DTO.CallbackMoRequest;
import com.vector.telefonia.DTO.RecivedRequestSMS;
import com.vector.telefonia.models.ContatoMalaDireta;
import com.vector.telefonia.models.CorpoSMS;
import com.vector.telefonia.models.RespostaCliente;
import com.vector.telefonia.models.RespostaSMS;
import com.vector.telefonia.service.ContatoService;
import com.vector.telefonia.service.SmsService;

@RestController
@RequestMapping("/SMS")
public class TelefoniaResource {
	
	@Autowired
	private ContatoService contatoService;
	
	@Autowired
	private SmsService smsService;
	
	/**
	 * Método que realiza o tratamento do recebimento de SMS e salva no banco a
	 * resposta do cliente e que vai ou não responder o mesmo.
	 * 
	 * @param JSON com os dados da resposta do cliente
	 * @return null
	 */
	@PostMapping("/recebimentoEnvioResposta")
	public void recebeResposta(@RequestBody RecivedRequestSMS recived) throws IOException, Exception { 
		CallbackMoRequest resposta = recived.getCallbackMoRequest();	
		if (resposta.getCorrelatedMessageSmsId() != null) {
			// BUSCA OS DADOS DO CONTATO QUE RESPONDEU
			Long cdContato = Long.valueOf(resposta.getCorrelatedMessageSmsId());
			ContatoMalaDireta contato = contatoService.getContatoMalaDiretaById(cdContato);
			CorpoSMS corpoSMS = smsService.getCorpoPerguntaSMS(contato.getCdCorpoSMS());
			// SALVA A RESPOSTA DO CLIENTE
			RespostaCliente respostaCliente = smsService.salvaRespostaCliente(contato, corpoSMS, resposta);
			RespostaSMS respostaSMS = smsService.getRespostaSMS(corpoSMS.getCdCorpo());
			// BUSCAR A PROXIMA MENSAGEM A SER ENVIADA CASO O A RESPOSTA ESTEJA COMO ESPERADA
			if (respostaSMS != null && respostaCliente != null) {
				if (respostaSMS.getDsCorpoSMS().toUpperCase().contains(respostaCliente.getDsCorpoCliente().toUpperCase())) {
					corpoSMS = smsService.getProximoCorpoPerguntaSMS(contato.getLote().getMalaDireta().getContato(), corpoSMS.getCdCorpo());
					if (corpoSMS != null) {
						contato = contatoService.saveContatoMalaDiretaRespostaSMS(contato, corpoSMS.getCdCorpo());
						// AQUI REENVIA A MESNAGEM PRO CLEINTE
						String response = smsService.enviarResposta(contato, corpoSMS);
						smsService.salvarStatusContatoEnvioSMS(contato, response, corpoSMS);
						
					}
				}
			}
		}
	}
}
