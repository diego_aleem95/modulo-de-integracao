package com.vector.telefonia.DTO;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vector.telefonia.models.ContatoMalaDireta;
import com.vector.telefonia.models.CorpoSMS;

public class SmsRequestData {
	private String id;
	@JsonProperty("from")
	private String from;
	@JsonProperty("to")
	private String to;
	@JsonProperty("schedule")
	private String schedule;
	@JsonProperty("msg")
	private String msg;
	@JsonProperty("callbackOption")
	private String callbackOption;
	@JsonProperty("aggregateId")
	private String aggregateId;
	@JsonProperty("flashSms")
	private Boolean flashSms;
	
	
	public SmsRequestData() {
	
	}
	public SmsRequestData(ContatoMalaDireta contato, CorpoSMS body) {
	  this.id =  String.valueOf(contato.getCdContato());	
	  this.to = "55"+contato.getTelefone();
	  this.msg = body.getDsCorpoSMS();
	  this.schedule = dateToIso8601Format(new Date());
	  this.callbackOption = "NONE";
	  this.aggregateId = String.valueOf(body.getCdCorpo());
	  this.flashSms = false;
	  
	}
	public String getRemetente() {
		return from;
	}
	public void setRemetente(String remetente) {
		this.from = remetente;
	}
	public String getDestinatario() {
		return to;
	}
	public void setDestinatario(String destinatario) {
		this.to = destinatario;
	}
	public String getDataDeEnvio() {
		return schedule;
	}
	public void setDataDeEnvio(String dataDeEnvio) {
		this.schedule = dataDeEnvio;
	}
	public String getMensagem() {
		return msg;
	}
	public void setMensagem(String mensagem) {
		this.msg = mensagem;
	}
	public String getCallback() {
		return callbackOption;
	}
	public void setCallback(String callback) {
		this.callbackOption = callback;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public static String dateToIso8601Format(Date date) {
		TimeZone tz = TimeZone.getTimeZone("America/Sao_Paulo");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		df.setTimeZone(tz);
		return df.format(date);
	}
	
	@Override
	public String toString() {
		return "SmsRequestData [id=" + id + ", from=" + from + ", to=" + to
				+ ", schedule=" + schedule + ", msg=" + msg + ", callbackOption=" + callbackOption + "]";
	}
}
