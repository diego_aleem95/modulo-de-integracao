package com.vector.telefonia.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RecivedRequestSMS {

	@JsonProperty("callbackMoRequest")
	private CallbackMoRequest callbackMoRequest;

	public CallbackMoRequest getCallbackMoRequest() {
		return callbackMoRequest;
	}

	public void setCallbackMoRequest(CallbackMoRequest callbackMoRequest) {
		this.callbackMoRequest = callbackMoRequest;
	}

	public RecivedRequestSMS(CallbackMoRequest callbackMoRequest) {
		super();
		this.callbackMoRequest = callbackMoRequest;
	}

	public RecivedRequestSMS() {
		super();
	}

}
