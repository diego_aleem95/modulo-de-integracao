package com.vector.telefonia.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SmsRequest {
	
	@JsonProperty("sendSmsRequest")
	private SmsRequestData sendSmsRequest;
	
	public SmsRequest(SmsRequestData sendSmsRequest) {
		super();
		this.sendSmsRequest = sendSmsRequest;
	}
	
	public SmsRequestData getDadosContato() {
		return sendSmsRequest;
	}
	public void setDadosContato(SmsRequestData sendSmsRequest) {
		this.sendSmsRequest = sendSmsRequest;
	}

	@Override
	public String toString() {
		return "SmsRequest [sendSmsRequest=" + sendSmsRequest + "]";
	}
	
}
