package com.vector.telefonia.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CallbackMoRequest {
	
	 @JsonProperty("id")
	 private String id;
	 
     @JsonProperty("mobile")
     private String mobile;
     
     @JsonProperty("shortCode")
     private String shortCode;
     
     @JsonProperty("account")
     private String account;
     
     @JsonProperty("body")
     private String body;
     
     @JsonProperty("received")
     private String received;
     
     @JsonProperty("correlatedMessageSmsId")
     private String correlatedMessageSmsId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getReceived() {
		return received;
	}

	public void setReceived(String received) {
		this.received = received;
	}

	public String getCorrelatedMessageSmsId() {
		return correlatedMessageSmsId;
	}

	public void setCorrelatedMessageSmsId(String correlatedMessageSmsId) {
		this.correlatedMessageSmsId = correlatedMessageSmsId;
	}

	public CallbackMoRequest() {
		super();
		
	}

	public CallbackMoRequest(String id, String mobile, String shortCode, String account, String body, String received,
			String correlatedMessageSmsId) {
		super();
		this.id = id;
		this.mobile = mobile;
		this.shortCode = shortCode;
		this.account = account;
		this.body = body;
		this.received = received;
		this.correlatedMessageSmsId = correlatedMessageSmsId;
	}
     
	
     
}
