package com.vector.telefonia.models;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema = "maladireta", name = "tb_contato")
public class ContatoMalaDireta {
	
	private static final long serialVersionUID = -4059937254719565622L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_contato")
	private Long cdContato;
	
	@ManyToOne
	@JoinColumn(name = "cd_lote", referencedColumnName = "cd_lote")
	private Lote lote;
	
	@ManyToOne
	@JoinColumn(name = "cd_status_contato", referencedColumnName = "cd_status_contato")
	private StatusContatoEmail statusContatoEmail;
	
	@Column(name = "cd_usuario_responsavel")
	private Long cdUsuarioResponsavel;
	
	@Column(name = "ds_email")
	private String email;
	
	@Column(name = "nm_contato")
	private String nome;
	
	@Column(name = "nm_cidade")
	private String cidade;
	
	@Column(name = "sg_estado")
	private String estado;
	
	@Column(name = "ds_cod_ibge")
	private String codIbge;
	
	@Column(name = "ds_tipo_publico")
	private String dsTipoPublico;
	
	@Column(name = "ds_orgao")
	private String dsOrgao;
	
	@Column(name = "in_erro")
	private char erro;
	
	@Column(name = "dt_tentativa")
	private Timestamp dtTentativa;
	
	@Column(name = "dt_auditoria")
	private Timestamp dtAuditoria;
	
	@Column(name = "ds_erro")
	private String dsErro;
	
	@Column(name = "nm_pergunta")
	private Long cdCorpoSMS;
	
	@ManyToOne
	@JoinColumn(name = "cd_status_sms", referencedColumnName = "id_status")
	private StatusSMS statusSMS;
	
	@Column(name = "nu_telefone")
	private BigInteger telefone;

	public Long getCdContato() {
		return cdContato;
	}

	public void setCdContato(Long cdContato) {
		this.cdContato = cdContato;
	}

	public Lote getLote() {
		return lote;
	}

	public void setLote(Lote lote) {
		this.lote = lote;
	}

	public StatusContatoEmail getStatusContatoEmail() {
		return statusContatoEmail;
	}

	public void setStatusContatoEmail(StatusContatoEmail statusContatoEmail) {
		this.statusContatoEmail = statusContatoEmail;
	}

	public Long getCdUsuarioResponsavel() {
		return cdUsuarioResponsavel;
	}

	public void setCdUsuarioResponsavel(Long cdUsuarioResponsavel) {
		this.cdUsuarioResponsavel = cdUsuarioResponsavel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodIbge() {
		return codIbge;
	}

	public void setCodIbge(String codIbge) {
		this.codIbge = codIbge;
	}

	public String getDsTipoPublico() {
		return dsTipoPublico;
	}

	public void setDsTipoPublico(String dsTipoPublico) {
		this.dsTipoPublico = dsTipoPublico;
	}

	public String getDsOrgao() {
		return dsOrgao;
	}

	public void setDsOrgao(String dsOrgao) {
		this.dsOrgao = dsOrgao;
	}

	public char getErro() {
		return erro;
	}

	public void setErro(char erro) {
		this.erro = erro;
	}

	public Timestamp getDtTentativa() {
		return dtTentativa;
	}

	public void setDtTentativa(Timestamp dtTentativa) {
		this.dtTentativa = dtTentativa;
	}

	public Timestamp getDtAuditoria() {
		return dtAuditoria;
	}

	public void setDtAuditoria(Timestamp dtAuditoria) {
		this.dtAuditoria = dtAuditoria;
	}
	

	public String getDsErro() {
		return dsErro;
	}

	public void setDsErro(String dsErro) {
		this.dsErro = dsErro;
	}

	public StatusSMS getStatusSMS() {
		return statusSMS;
	}

	public void setStatusSMS(StatusSMS statusSMS) {
		this.statusSMS = statusSMS;
	}

	public BigInteger getTelefone() {
		return telefone;
	}

	public void setTelefone(BigInteger telefone) {
		this.telefone = telefone;
	}

	public Long getCdCorpoSMS() {
		return cdCorpoSMS;
	}

	public void setCdCorpoSMS(Long cdCorpoSMS) {
		this.cdCorpoSMS = cdCorpoSMS;
	}

	public ContatoMalaDireta(Long cdContato, Lote lote, StatusContatoEmail statusContatoEmail, Long cdUsuarioResponsavel,
			String email, String nome, String cidade, String estado, String codIbge, String dsTipoPublico,
			String dsOrgao, char erro, Timestamp dtTentativa, Timestamp dtAuditoria) {
		super();
		this.cdContato = cdContato;
		this.lote = lote;
		this.statusContatoEmail = statusContatoEmail;
		this.cdUsuarioResponsavel = cdUsuarioResponsavel;
		this.email = email;
		this.nome = nome;
		this.cidade = cidade;
		this.estado = estado;
		this.codIbge = codIbge;
		this.dsTipoPublico = dsTipoPublico;
		this.dsOrgao = dsOrgao;
		this.erro = erro;
		this.dtTentativa = dtTentativa;
		this.dtAuditoria = dtAuditoria;
	}

	public ContatoMalaDireta() {
		super();
	}

	public ContatoMalaDireta(String nome, String tipoPublico, String orgao, String uf, String municipio, String codIBGE, String email) {
		this.nome = nome;
		this.dsTipoPublico = tipoPublico;
		this.dsOrgao = orgao;
		this.estado = uf;
		this.cidade = municipio;
		this.codIbge = codIBGE;
		this.email = email;
	}
	
	
	public ContatoMalaDireta(String string, String string2, String string3, String string4, String string5, String string6) {
		super();
		this.nome = string;
		this.dsTipoPublico = string2;
		this.dsOrgao = string3;
		this.estado = string4;
		this.cidade = string5;
		this.codIbge = string6;
		this.dtAuditoria = new Timestamp(new Date().getTime());
	}

	public ContatoMalaDireta(String string, String string2, String string3, String string4, String string5,
			String string6, String string7, String string8) {
		super();
		this.nome = string;
		this.dsTipoPublico = string2;
		this.dsOrgao = string3;
		this.estado = string4;
		this.cidade = string5;
		this.codIbge = string6;
		this.dtAuditoria = new Timestamp(new Date().getTime());
		String tel = string7+string8;
		this.telefone = new BigInteger(tel);
	
	}

	public ContatoMalaDireta(ContatoMalaDireta contato) {
		this.lote = contato.getLote();
		this.statusContatoEmail = contato.getStatusContatoEmail();
		this.cdUsuarioResponsavel = contato.getCdUsuarioResponsavel();
		this.email = contato.getEmail();
		this.nome = contato.getNome();
		this.cidade = contato.getCidade();
		this.estado = contato.getEstado();
		this.codIbge = contato.getCodIbge();
		this.dsTipoPublico = contato.getDsTipoPublico();
		this.dsOrgao = contato.getDsOrgao();
		this.erro = contato.getErro();
		this.dtTentativa = new Timestamp(new Date().getTime());
		this.dtAuditoria = new Timestamp(new Date().getTime());;
		this.dsErro = contato.getDsErro();
		this.cdCorpoSMS = contato.getCdCorpoSMS();
		this.statusSMS = contato.getStatusSMS();
		this.telefone = contato.getTelefone();
	}	
	

}