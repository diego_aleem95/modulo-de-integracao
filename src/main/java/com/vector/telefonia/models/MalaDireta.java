package com.vector.telefonia.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(schema = "maladireta", name = "tb_maladireta")
public class MalaDireta implements Serializable{

	private static final long serialVersionUID = -6170940774556873950L;
	
	@Id
	@Column(name="cd_maladireta")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long contato;
	
	@ManyToOne
	@JoinColumn(name="cd_status_maladireta", referencedColumnName="cd_status_maladireta")
	private StatusMalaDireta statusMalaDireta;
	
	@Column(name="cd_secretaria_setor")
	private Long cdSecretariaSetor;
	
	@Column(name="cd_programa_projeto")
	private Long cdProgramaProjeto;
	
	@SequenceGenerator(name = "protocolo_seq", sequenceName = "maladireta.seq_num_protocolo_tb_maladireta", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "protocolo_seq")
	@Column(name = "nu_protocolo_maladireta",unique = true, insertable = false,nullable = false, precision = 10, scale = 0)
	private String nuProtocolo;
	
	@Column(name="nm_maladireta")
	private String nmMalaDireta;
	
	@Column(name = "nm_responsavel_area_tecnica")
	private String nmResponsavelAreaTecnica;
	
	@Column(name = "ds_maladireta")
	private String dsMaladireta;
	
	@Column(name = "dt_criacao")
	private Timestamp dtCriacao;
	
	@Column(name = "dt_cancelamento")
	private Timestamp dtCancelamento;
	
	@Column(name = "dt_auditoria")
	private Timestamp dtAuditoria;
	
	@Column(name = "ds_mensagem")
	private String dsMensagem;
	
	@Column(name = "cd_ususario_criacao")
	private Long cdUsusarioCriacao;
	  
	@Column(name = "cd_usuario_cancelamento")
	private Long cdUsusarioCancelamento;
	
	@Column(name = "cd_usuario_supervisor")
	private Long cdUsusarioSupervisor;
	
	@Column(name = "cd_caixa_email")
	private Long cdCaixaEmail;
	
	@Column(name = "cdGrupo")
	private Long cdGrupo;
	
	@Column(name = "assunto")
	private String assunto;
	
	@Column(name = "cd_tipo")
	private Long cdTipo;
	
	@Column(name = "cd_campanha_voz")
	private Long cdCampanhaVoz;

	public Long getContato() {
		return contato;
	}

	public void setContato(Long contato) {
		this.contato = contato;
	}

	public StatusMalaDireta getStatusMalaDireta() {
		return statusMalaDireta;
	}

	public void setStatusMalaDireta(StatusMalaDireta statusMalaDireta) {
		this.statusMalaDireta = statusMalaDireta;
	}

	public Long getCdSecretariaSetor() {
		return cdSecretariaSetor;
	}

	public void setCdSecretariaSetor(Long cdSecretariaSetor) {
		this.cdSecretariaSetor = cdSecretariaSetor;
	}

	public Long getCdProgramaProjeto() {
		return cdProgramaProjeto;
	}

	public void setCdProgramaProjeto(Long cdProgramaProjeto) {
		this.cdProgramaProjeto = cdProgramaProjeto;
	}

	public String getNuProtocolo() {
		return nuProtocolo;
	}

	public void setNuProtocolo(String nuProtocolo) {
		this.nuProtocolo = nuProtocolo;
	}

	public String getNmMalaDireta() {
		return nmMalaDireta;
	}

	public void setNmMalaDireta(String nmMalaDireta) {
		this.nmMalaDireta = nmMalaDireta;
	}

	public String getNmResponsavelAreaTecnica() {
		return nmResponsavelAreaTecnica;
	}

	public void setNmResponsavelAreaTecnica(String nmResponsavelAreaTecnica) {
		this.nmResponsavelAreaTecnica = nmResponsavelAreaTecnica;
	}

	public String getDsMaladireta() {
		return dsMaladireta;
	}

	public void setDsMaladireta(String dsMaladireta) {
		this.dsMaladireta = dsMaladireta;
	}

	public Timestamp getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Timestamp dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	public Timestamp getDtCancelamento() {
		return dtCancelamento;
	}

	public void setDtCancelamento(Timestamp dtCancelamento) {
		this.dtCancelamento = dtCancelamento;
	}

	public Timestamp getDtAuditoria() {
		return dtAuditoria;
	}

	public void setDtAuditoria(Timestamp dtAuditoria) {
		this.dtAuditoria = dtAuditoria;
	}

	public String getDsMensagem() {
		return dsMensagem;
	}

	public void setDsMensagem(String dsMensagem) {
		this.dsMensagem = dsMensagem;
	}

	public Long getCdUsusarioCriacao() {
		return cdUsusarioCriacao;
	}

	public void setCdUsusarioCriacao(Long cdUsusarioCriacao) {
		this.cdUsusarioCriacao = cdUsusarioCriacao;
	}

	public Long getCdUsusarioCancelamento() {
		return cdUsusarioCancelamento;
	}

	public void setCdUsusarioCancelamento(Long cdUsusarioCancelamento) {
		this.cdUsusarioCancelamento = cdUsusarioCancelamento;
	}

	public Long getCdUsusarioSupervisor() {
		return cdUsusarioSupervisor;
	}

	public void setCdUsusarioSupervisor(Long cdUsusarioSupervisor) {
		this.cdUsusarioSupervisor = cdUsusarioSupervisor;
	}

	public Long getCdCaixaEmail() {
		return cdCaixaEmail;
	}

	public void setCdCaixaEmail(Long cdCaixaEmail) {
		this.cdCaixaEmail = cdCaixaEmail;
	}

	public Long getCdGrupo() {
		return cdGrupo;
	}

	public void setCdGrupo(Long cdGrupo) {
		this.cdGrupo = cdGrupo;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public Long getCdTipo() {
		return cdTipo;
	}

	public void setCdTipo(Long cdTipo) {
		this.cdTipo = cdTipo;
	}

	public Long getCdCampanhaVoz() {
		return cdCampanhaVoz;
	}

	public void setCdCampanhaVoz(Long cdCampanhaVoz) {
		this.cdCampanhaVoz = cdCampanhaVoz;
	}

	public MalaDireta(Long contato, StatusMalaDireta statusMalaDireta, Long cdSecretariaSetor, Long cdProgramaProjeto,
			String nuProtocolo, String nmMalaDireta, String nmResponsavelAreaTecnica, String dsMaladireta,
			Timestamp dtCriacao, Timestamp dtCancelamento, Timestamp dtAuditoria, String dsMensagem,
			Long cdUsusarioCriacao, Long cdUsusarioCancelamento, Long cdUsusarioSupervisor, Long cdCaixaEmail,
			Long cdGrupo, String assunto, Long cdTipo, Long cdCampanhaVoz) {
		super();
		this.contato = contato;
		this.statusMalaDireta = statusMalaDireta;
		this.cdSecretariaSetor = cdSecretariaSetor;
		this.cdProgramaProjeto = cdProgramaProjeto;
		this.nuProtocolo = nuProtocolo;
		this.nmMalaDireta = nmMalaDireta;
		this.nmResponsavelAreaTecnica = nmResponsavelAreaTecnica;
		this.dsMaladireta = dsMaladireta;
		this.dtCriacao = dtCriacao;
		this.dtCancelamento = dtCancelamento;
		this.dtAuditoria = dtAuditoria;
		this.dsMensagem = dsMensagem;
		this.cdUsusarioCriacao = cdUsusarioCriacao;
		this.cdUsusarioCancelamento = cdUsusarioCancelamento;
		this.cdUsusarioSupervisor = cdUsusarioSupervisor;
		this.cdCaixaEmail = cdCaixaEmail;
		this.cdGrupo = cdGrupo;
		this.assunto = assunto;
		this.cdTipo = cdTipo;
		this.cdCampanhaVoz = cdCampanhaVoz;
	}

	public MalaDireta() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
