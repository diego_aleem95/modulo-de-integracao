package com.vector.telefonia.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "maladireta", name = "tb_status_sms")
public class StatusSMS {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_status")
	private Long idStatus;
	
	@Column(name = "no_status")
	private String noStatus;

	public Long getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(Long idStatus) {
		this.idStatus = idStatus;
	}

	public String getNoStatus() {
		return noStatus;
	}

	public void setNoStatus(String noStatus) {
		this.noStatus = noStatus;
	}

	public StatusSMS() {
		super();
	}

	public StatusSMS(Long idStatus, String noStatus) {
		super();
		this.idStatus = idStatus;
		this.noStatus = noStatus;
	}
	
	
}
