package com.vector.telefonia.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "maladireta", name = "tb_status_maladireta")
public class StatusMalaDireta implements Serializable {

	private static final long serialVersionUID = -964748238770438691L;

	@Id
	@Column(name="cd_status_maladireta")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long cdStatusMaladireta;
	
	@Column(name="nm_status")
	private String nmStatus;
	
	public StatusMalaDireta() {
		super();
	}

	public StatusMalaDireta(Long cdStatusMaladireta, String nmStatus) {
		super();
		this.cdStatusMaladireta = cdStatusMaladireta;
		this.nmStatus = nmStatus;
	}

	public Long getCdStatusMaladireta() {
		return cdStatusMaladireta;
	}

	public void setCdStatusMaladireta(Long cdStatusMaladireta) {
		this.cdStatusMaladireta = cdStatusMaladireta;
	}

	public String getNmStatus() {
		return nmStatus;
	}

	public void setNmStatus(String nmStatus) {
		this.nmStatus = nmStatus;
	}
	
	
}
