package com.vector.telefonia.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="tb_resposta_sms", schema="maladireta")
public class RespostaSMS implements Serializable {

	private static final long serialVersionUID = 1070721684797979173L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="cd_resposta")
	private Long cdResposta;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="cd_corpo_sms", referencedColumnName="cd_pergunta")
	private CorpoSMS corpo;
	
	@Column(name="ds_resposta_sms")
	private String dsCorpoSMS;
	
	public RespostaSMS() {
		super();
	}

	public RespostaSMS(Long cdResposta, CorpoSMS corpo, String dsCorpoSMS) {
		super();
		this.cdResposta = cdResposta;
		this.corpo = corpo;
		this.dsCorpoSMS = dsCorpoSMS;
	}

	public Long getCdResposta() {
		return cdResposta;
	}

	public void setCdResposta(Long cdResposta) {
		this.cdResposta = cdResposta;
	}

	public CorpoSMS getCorpo() {
		return corpo;
	}

	public void setCorpo(CorpoSMS corpo) {
		this.corpo = corpo;
	}

	public String getDsCorpoSMS() {
		return dsCorpoSMS;
	}

	public void setDsCorpoSMS(String dsCorpoSMS) {
		this.dsCorpoSMS = dsCorpoSMS;
	}

}
