package com.vector.telefonia.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="tb_corpo_sms", schema="maladireta")
public class CorpoSMS implements Serializable {

	private static final long serialVersionUID = 1070721684797979173L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="cd_pergunta")
	private Long cdCorpo;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="cd_maladireta", referencedColumnName="cd_maladireta")
	private MalaDireta malaDireta;
	
	@Column(name="ds_corpo_sms")
	private String dsCorpoSMS;

	public Long getCdCorpo() {
		return cdCorpo;
	}

	public void setCdCorpo(Long cdCorpo) {
		this.cdCorpo = cdCorpo;
	}

	public MalaDireta getMalaDireta() {
		return malaDireta;
	}

	public void setMalaDireta(MalaDireta malaDireta) {
		this.malaDireta = malaDireta;
	}

	public String getDsCorpoSMS() {
		return dsCorpoSMS;
	}

	public void setDsCorpoSMS(String dsCorpoSMS) {
		this.dsCorpoSMS = dsCorpoSMS;
	}

	public CorpoSMS(Long cdCorpo, MalaDireta malaDireta, String dsCorpoSMS) {
		super();
		this.cdCorpo = cdCorpo;
		this.malaDireta = malaDireta;
		this.dsCorpoSMS = dsCorpoSMS;
	}

	public CorpoSMS() {
		super();
	}

}
