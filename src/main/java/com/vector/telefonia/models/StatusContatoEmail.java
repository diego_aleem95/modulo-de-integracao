package com.vector.telefonia.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "maladireta", name = "tb_status_contato")
public class StatusContatoEmail implements Serializable {

	private static final long serialVersionUID = 4882896599138225410L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cd_status_contato")
	private Long cdStatusContato;
	
	@Column(name = "nm_status")
	private String nomeStatus;

	public Long getCdStatusContato() {
		return cdStatusContato;
	}

	public void setCdStatusContato(Long cdStatusContato) {
		this.cdStatusContato = cdStatusContato;
	}

	public String getNomeStatus() {
		return nomeStatus;
	}

	public void setNomeStatus(String nomeStatus) {
		this.nomeStatus = nomeStatus;
	}

	public StatusContatoEmail() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StatusContatoEmail(Long cdStatusContato, String nomeStatus) {
		super();
		this.cdStatusContato = cdStatusContato;
		this.nomeStatus = nomeStatus;
	}
	
	
	
}
