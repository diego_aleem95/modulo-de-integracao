package com.vector.telefonia.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema = "maladireta", name = "tb_lote")
public class Lote implements Serializable{

	private static final long serialVersionUID = -4778527780538231120L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_lote")
	private Long cdLote;
	
	@ManyToOne
	@JoinColumn(name = "cd_maladireta", referencedColumnName = "cd_maladireta")
	private MalaDireta malaDireta;
	
	@ManyToOne
	@JoinColumn(name = "cd_status_lote", referencedColumnName = "cd_status_lote")
	private StatusLote statusLote;
	
	@Column(name = "dt_inicio_execucao")
	private Timestamp dtInicio;
	
	@Column(name = "dt_fim_execucao")
	private Timestamp dtFim;
	
	@Column(name = "dt_auditoria")
	private Timestamp dtAuditoria;

	@Column(name = "num_thread")
	private String nuThread;

	public Long getCdLote() {
		return cdLote;
	}

	public void setCdLote(Long cdLote) {
		this.cdLote = cdLote;
	}

	public MalaDireta getMalaDireta() {
		return malaDireta;
	}

	public void setMalaDireta(MalaDireta malaDireta) {
		this.malaDireta = malaDireta;
	}

	public StatusLote getStatusLote() {
		return statusLote;
	}

	public void setStatusLote(StatusLote statusLote) {
		this.statusLote = statusLote;
	}

	public Timestamp getDtInicio() {
		return dtInicio;
	}

	public void setDtInicio(Timestamp dtInicio) {
		this.dtInicio = dtInicio;
	}

	public Timestamp getDtFim() {
		return dtFim;
	}

	public void setDtFim(Timestamp dtFim) {
		this.dtFim = dtFim;
	}

	public Timestamp getDtAuditoria() {
		return dtAuditoria;
	}

	public void setDtAuditoria(Timestamp dtAuditoria) {
		this.dtAuditoria = dtAuditoria;
	}

	public String getNuThread() {
		return nuThread;
	}

	public void setNuThread(String nuThread) {
		this.nuThread = nuThread;
	}

	public Lote(Long cdLote, MalaDireta malaDireta, StatusLote statusLote, Timestamp dtInicio, Timestamp dtFim,
			Timestamp dtAuditoria, String nuThread) {
		super();
		this.cdLote = cdLote;
		this.malaDireta = malaDireta;
		this.statusLote = statusLote;
		this.dtInicio = dtInicio;
		this.dtFim = dtFim;
		this.dtAuditoria = dtAuditoria;
		this.nuThread = nuThread;
	}

	public Lote() {
		super();
	}
	
	
}
