package com.vector.telefonia.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "maladireta", name = "tb_status_lote")
public class StatusLote implements Serializable{

	private static final long serialVersionUID = 7837203542836999977L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cd_status_lote")
	private Long cdStatusMalaDireta;
	
	@Column(name = "nm_status")
	private String nmStatus;

	public Long getCdStatusMalaDireta() {
		return cdStatusMalaDireta;
	}

	public void setCdStatusMalaDireta(Long cdStatusMalaDireta) {
		this.cdStatusMalaDireta = cdStatusMalaDireta;
	}

	public String getNmStatus() {
		return nmStatus;
	}

	public void setNmStatus(String nmStatus) {
		this.nmStatus = nmStatus;
	}

	public StatusLote(Long cdStatusMalaDireta, String nmStatus) {
		super();
		this.cdStatusMalaDireta = cdStatusMalaDireta;
		this.nmStatus = nmStatus;
	}

	public StatusLote() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
