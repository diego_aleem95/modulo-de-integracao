package com.vector.telefonia.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="tb_resposta_contato", schema="maladireta")
public class RespostaCliente implements Serializable {

	private static final long serialVersionUID = 1070721684797979173L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="cd_resposta_contato")
	private Long cdRespostaContato;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="cd_corpo_sms", referencedColumnName="cd_pergunta")
	private CorpoSMS corpo;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="cd_contato", referencedColumnName="cd_contato")
	private ContatoMalaDireta contato;
	
	@Column(name="ds_resposta_cliente")
	private String dsCorpoCliente;
	
	@Column(name = "dt_recebida")
	private Timestamp dtRecebimento;
	

	public Timestamp getDtRecebimento() {
		return dtRecebimento;
	}

	public void setDtRecebimento(Timestamp dtRecebimento) {
		this.dtRecebimento = dtRecebimento;
	}

	public Long getCdRespostaContato() {
		return cdRespostaContato;
	}

	public void setCdRespostaContato(Long cdRespostaContato) {
		this.cdRespostaContato = cdRespostaContato;
	}

	public CorpoSMS getCorpo() {
		return corpo;
	}

	public void setCorpo(CorpoSMS corpo) {
		this.corpo = corpo;
	}

	public ContatoMalaDireta getContato() {
		return contato;
	}

	public void setContato(ContatoMalaDireta contato) {
		this.contato = contato;
	}

	public String getDsCorpoCliente() {
		return dsCorpoCliente;
	}

	public void setDsCorpoCliente(String dsCorpoCliente) {
		this.dsCorpoCliente = dsCorpoCliente;
	}

	public RespostaCliente(Long cdRespostaContato, CorpoSMS corpo, ContatoMalaDireta contato, String dsCorpoCliente,
			Timestamp dtRecebimento) {
		super();
		this.cdRespostaContato = cdRespostaContato;
		this.corpo = corpo;
		this.contato = contato;
		this.dsCorpoCliente = dsCorpoCliente;
		this.dtRecebimento = dtRecebimento;
	}

	public RespostaCliente() {
		super();
	
	}
	
}
