package com.vector.telefonia.service;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vector.telefonia.DTO.CallbackMoRequest;
import com.vector.telefonia.DTO.SmsRequest;
import com.vector.telefonia.DTO.SmsRequestData;
import com.vector.telefonia.models.ContatoMalaDireta;
import com.vector.telefonia.models.CorpoSMS;
import com.vector.telefonia.models.RespostaCliente;
import com.vector.telefonia.models.RespostaSMS;
import com.vector.telefonia.models.StatusContatoEmail;
import com.vector.telefonia.models.StatusSMS;
import com.vector.telefonia.repository.ContatoEmailRepository;
import com.vector.telefonia.repository.CorpoSMSRepository;
import com.vector.telefonia.repository.RespostaClienteRepository;
import com.vector.telefonia.repository.RespostaSMSRepository;
import com.vector.telefonia.repository.StatusContatoEmailRepository;
import com.vector.telefonia.repository.StatusSMSRepository;
import com.vector.telefonia.utils.AtivoUtils;


@Service
public class SmsService {
		
	@Autowired
	private CorpoSMSRepository corpoSMSRepository;
	
	@Autowired
	private RespostaSMSRepository respostaSMSRepository;
	
	@Autowired
	private RespostaClienteRepository respostaClienteRepository;
	
	@Autowired
	private StatusSMSRepository statusSMSRepository;
	
	@Autowired
	private StatusContatoEmailRepository statusContatoEmailRepository;
	
	@Autowired
	private ContatoEmailRepository contatoEmailRepository;
	
	private final Long CONTATO_MALA_DIRETA_ENVIADO = new Long("2");


	public CorpoSMS getCorpoPerguntaSMS(Long cdCorpo) {
		CorpoSMS corpoSMS = corpoSMSRepository.findById(cdCorpo).get();
		return corpoSMS;
	}

	public CorpoSMS getProximoCorpoPerguntaSMS(Long cdAtivo, Long cdCorpo) {
		Integer idCorpo = corpoSMSRepository.getProximoCorpoPerguntaSMS(cdAtivo, cdCorpo);
		if(idCorpo != null) {
			Long id = Long.valueOf(String.valueOf(idCorpo));
			CorpoSMS corpoSMS = corpoSMSRepository.findById(id).get();
			return corpoSMS;
		}
		return null;
	}

	public RespostaSMS getRespostaSMS(Long cdCorpo) {
		Long cdResposta = respostaSMSRepository.getCdMalaDireta(cdCorpo);
		RespostaSMS respostaSMS = respostaSMSRepository.findById(cdResposta).get();
		return respostaSMS;
	}
	
	public String enviarResposta(ContatoMalaDireta contato, CorpoSMS resposta) {
		Gson gson = new Gson();  
		try {
			//Serão add todos os contatos por lote 
			SmsRequestData contatoSmsRequestData = new SmsRequestData(contato, resposta);
			SmsRequest SmsRequest = new SmsRequest(contatoSmsRequestData);
			String json = gson.toJson(SmsRequest);
			String ret = AtivoUtils.requestPostToZenviaAPI(json);
			JsonParser parse = new JsonParser();
			JsonObject jsonObject = (JsonObject)parse.parse(ret);
			Long cdStatusSms = Long.valueOf(jsonObject.getAsJsonObject("sendSmsResponse").get("detailCode").getAsString());
			StatusSMS statusSMS = statusSMSRepository.findById(cdStatusSms).get();
			contato.setStatusSMS(statusSMS);
			contato.setDtTentativa(new Timestamp(new Date().getTime()));
			StatusContatoEmail statusContatoEmail = statusContatoEmailRepository.findById(CONTATO_MALA_DIRETA_ENVIADO).get();
			contato.setStatusContatoEmail(statusContatoEmail);
			contato.setCdCorpoSMS(resposta.getCdCorpo());
			contatoEmailRepository.save(contato);
			System.out.println("RESPOSTA ENVIADA PARA O CLIENTE: "+contato.getCdContato());
			return statusSMS.getNoStatus();
	
		} catch (Exception e) {
			System.out.println("ERRO AO ENVIAR RESPOSTA PARA O CLIENTE: "+contato.getCdContato());
			return null;
		}
	}

	public RespostaCliente salvaRespostaCliente(ContatoMalaDireta contato, CorpoSMS corpoSMS, CallbackMoRequest resposta) {
		try {
			RespostaCliente respostaCliente = new RespostaCliente();
			respostaCliente.setContato(contato);
			respostaCliente.setCorpo(corpoSMS);
			respostaCliente.setDsCorpoCliente(resposta.getBody());
			respostaCliente.setDtRecebimento(new Timestamp(new Date().getTime()));
			respostaClienteRepository.save(respostaCliente);
			System.out.print("A RESPOSTA DO CLIENTE FOI SALVA: "+contato.getCdContato());
			return respostaCliente;
		} catch (Exception e) {
			System.out.print("NÃO FOI SALVA A RESPOSTA DO CLIENTE: "+contato.getCdContato()+" || "+e);
			return null;
		}
	}

	public void salvarStatusContatoEnvioSMS(ContatoMalaDireta contato, String ret, CorpoSMS corpoSMS) {
		JsonParser parse = new JsonParser();
		JsonObject jsonObject = (JsonObject)parse.parse(ret);
		Long cdStatusSms = Long.valueOf(jsonObject.getAsJsonObject("sendSmsResponse").get("detailCode").getAsString());
		StatusSMS statusSMS = statusSMSRepository.findById(cdStatusSms).get();
		contato.setStatusSMS(statusSMS);
		contato.setDtTentativa(new Timestamp(new Date().getTime()));
		StatusContatoEmail statusContatoEmail = statusContatoEmailRepository.findById(CONTATO_MALA_DIRETA_ENVIADO).get();
		contato.setStatusContatoEmail(statusContatoEmail);
		contato.setCdCorpoSMS(corpoSMS.getCdCorpo());
		contatoEmailRepository.save(contato);
	}
}
