package com.vector.telefonia.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vector.telefonia.models.ContatoMalaDireta;
import com.vector.telefonia.models.StatusContatoEmail;
import com.vector.telefonia.repository.ContatoEmailRepository;
import com.vector.telefonia.repository.StatusContatoEmailRepository;


@Service
public class ContatoService {

	@Autowired
	private ContatoEmailRepository contatoEmailRepository;
	
	@Autowired
	private StatusContatoEmailRepository statusContatoEmailRepository;

	
	private final Long CONTATO_MALA_DIRETA_NAO_ENVIADO = new Long("1");

	
	public ContatoMalaDireta getContatoMalaDiretaById(Long cdContato) {
		ContatoMalaDireta contato = contatoEmailRepository.findById(cdContato).get();
		return contato;
	}
	
	public void saveContatoMalaDireta(ContatoMalaDireta contato) {
		contatoEmailRepository.save(contato);
	}
	
	public ContatoMalaDireta saveContatoMalaDiretaRespostaSMS(ContatoMalaDireta contato, Long cdCorpo) {
		StatusContatoEmail statusContatoEmail = statusContatoEmailRepository.findById(CONTATO_MALA_DIRETA_NAO_ENVIADO).get();
		contato.setCdCorpoSMS(cdCorpo);
		contato.setStatusContatoEmail(statusContatoEmail);
		ContatoMalaDireta novoContato = new ContatoMalaDireta(contato);
		novoContato.setStatusSMS(null);
		this.saveContatoMalaDireta(novoContato);
		return novoContato;
	}

}
