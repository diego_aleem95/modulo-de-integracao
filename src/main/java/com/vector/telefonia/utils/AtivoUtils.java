package com.vector.telefonia.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

public class AtivoUtils {

	/**
	 * Envia a requisição POST à API Zenvia
	 * 
	 * @param jsonObject string JSON que será enviado no corpo da requisição
	 * @return string JSON com a resposta da requisição
	 */
	public static String requestPostToZenviaAPI(String jsonObject) {
		try {
			URL url = new URL("https://api-rest.zenvia360.com.br/services/send-sms/");
			String token = "Basic dmVjdG9yLmNvcnA6bUZjdkkyRkZYSA==";

			HttpURLConnection con = setConnection(url, token);

			sendRequest(con, jsonObject);

			System.out.println("Response code: " + con.getResponseCode());

			if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
				StringBuffer content = writeResponse(con);

				if (content != null) {
					return content.toString();
				}

			} else {
				System.out.println("Erro na requisição à API Zenvia.");
				System.out.println(jsonObject);
				System.out.println(con.getResponseMessage());
			}
		} catch (ProtocolException p) {
			return null;
		} catch (IOException ex) {
			return null;
		} catch (NullPointerException ex) {
			return null;
		}

		return null;
	}

	/**
	 * Envia uma requisição com base na configuração do objeto HttpURLConnection
	 * 
	 * @param con        objeto responsável por realizar a conexão HTTP
	 * @param jsonObject string no formato JSON que irá no corpo da requisição
	 * @return código de resposta da requisição ou -1 caso alguma exceção ocorra
	 */
	public static void sendRequest(HttpURLConnection con, String jsonObject) {
		try (OutputStream os = con.getOutputStream();
				BufferedWriter output = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));) {
			output.write(jsonObject);
			output.flush();
			output.close();

			con.connect();
		} catch (IOException ex) {
			System.out.println("Houve um problema ao enviar a requisição:  " + ex + " URL: " + con);
		}
	}
	
	/**
	 * Seta o objeto que irá realizar a requisição POST à API Zenvia
	 * 
	 * @param url
	 *            URL do recurso aonde a requisição será realizada
	 * @param token
	 *            chave Base64 para autorização
	 * @return objeto responsável por realizar a conexão HTTP
	 */
	public static HttpURLConnection setConnection(URL url, String token) throws IOException {
		HttpURLConnection con = (HttpURLConnection) url.openConnection();

		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		con.setRequestProperty("Authorization", token);
		con.setRequestProperty("Accept", "application/json");

		con.setDoOutput(true);
		con.setDoInput(true);

		return con;
	}
	
	/**
	 * Escreve a resposta da requisição em um objeto StringBuffer
	 * 
	 * @param con
	 *            objeto que realiza a conexão HTTP
	 * @return StringBuffer com a resposta da requisição
	 */
	public static StringBuffer writeResponse(HttpURLConnection con) {
		try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
			String inputLine;
			StringBuffer content = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}

			return content;
		} catch (IOException ex) {
			return null;
		}
	}

}
