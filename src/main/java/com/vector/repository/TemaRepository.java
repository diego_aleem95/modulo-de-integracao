package com.vector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.model.Tema;


public interface TemaRepository extends JpaRepository<Tema,Long>{ 

	public List<Tema> findByStatus(boolean status);
}
