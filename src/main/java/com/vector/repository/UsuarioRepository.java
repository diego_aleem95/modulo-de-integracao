package com.vector.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario,Long>{
	
	public Optional<Usuario> findByDsLogin(String dsLogin);

	
}
