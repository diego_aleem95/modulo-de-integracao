package com.vector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.model.AlternativaPergunta;

public interface AlternativaPerguntaRepository extends JpaRepository<AlternativaPergunta, Long>{
	
	public List<AlternativaPergunta>findByCdPergunta(Long cdPergunta);
}
