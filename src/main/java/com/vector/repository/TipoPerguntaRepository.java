package com.vector.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.model.TipoPergunta;

public interface TipoPerguntaRepository extends JpaRepository<TipoPergunta, Long> {
	
}
