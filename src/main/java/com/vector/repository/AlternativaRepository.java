package com.vector.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.model.Alternativa;


public interface AlternativaRepository extends JpaRepository<Alternativa, Long>{
	
}
