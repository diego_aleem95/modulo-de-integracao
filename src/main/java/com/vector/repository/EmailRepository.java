package com.vector.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.model.Email;

public interface EmailRepository extends JpaRepository<Email, Long>{
	
		Email findByCdEmail(Long cdEmail);
}
