package com.vector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vector.model.Atendimento;


public interface AtendimentoRepository extends JpaRepository<Atendimento,Long> {
	
	public List<Atendimento> findByCdChamado(Long cdChamado);

	
	@Query(
			  value = "SELECT * FROM crm.tb_atendimento order by cd_atendimento DESC limit 350", 
			  nativeQuery = true)
	public List<Atendimento> getLastAtendimentos();
}
