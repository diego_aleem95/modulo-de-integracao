package com.vector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.model.MsgChat;

public interface MsgChatRepository extends JpaRepository<MsgChat, Long> {
	
	List<MsgChat> findByChatId(Long idChat);
}
