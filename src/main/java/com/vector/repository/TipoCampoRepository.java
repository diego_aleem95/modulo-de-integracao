package com.vector.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.model.TipoCampo;

public interface TipoCampoRepository extends JpaRepository<TipoCampo, Long>{

}
