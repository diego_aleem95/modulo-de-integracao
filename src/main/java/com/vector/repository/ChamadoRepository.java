package com.vector.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vector.model.Chamado;

public interface ChamadoRepository extends JpaRepository<Chamado, Long> {
	@Query(
			  value = "SELECT * FROM crm.tb_chamado c WHERE c.cd_ramal = ?1 order by cd_chamado DESC limit 1", 
			  nativeQuery = true)
	Chamado findUltimoChamadoOperador(Integer ramal);
}
