package com.vector.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.model.Grupo;

public interface GrupoRepository extends JpaRepository<Grupo, Long> {

}
