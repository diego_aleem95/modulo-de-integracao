package com.vector.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.model.Pergunta;

public interface PerguntaRepository extends JpaRepository<Pergunta, Long> {

}
