
package com.vector.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.model.Formulario;

public interface FormularioRepository extends JpaRepository<Formulario, Long> {
	
	Page<Formulario> findAllByStatus(Boolean status,Pageable pageable);
	
}
