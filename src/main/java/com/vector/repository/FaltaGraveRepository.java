package com.vector.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.model.FaltaGrave;

public interface FaltaGraveRepository extends JpaRepository<FaltaGrave, Long> {

}
