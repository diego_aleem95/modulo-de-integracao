package com.vector.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.model.Cdr;


public interface CdrRepository extends JpaRepository<Cdr,Long> {
	
	public ArrayList<Cdr> findByUniqueid(String uniqueid);
}
