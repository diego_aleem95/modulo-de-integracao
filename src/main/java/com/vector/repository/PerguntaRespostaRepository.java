package com.vector.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.model.PerguntaResposta;

public interface PerguntaRespostaRepository extends JpaRepository<PerguntaResposta, Long>{

	public List<PerguntaResposta>findByCdAvaliacao(Long cdAvaliacao);
}
