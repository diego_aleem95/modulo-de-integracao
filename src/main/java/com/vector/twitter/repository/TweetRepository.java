package com.vector.twitter.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vector.twitter.models.Tweet;

public interface TweetRepository extends JpaRepository<Tweet, Long>{

}
