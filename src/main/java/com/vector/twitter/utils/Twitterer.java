package com.vector.twitter.utils;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vector.twitter.models.Tweet;

import emoji4j.EmojiUtils;
import twitter4j.DirectMessage;
import twitter4j.DirectMessageList;
import twitter4j.GeoLocation;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

public class Twitterer {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private Twitter twitter;
	private PrintStream consolePrint;
	private List<Status> status;

	public Twitterer(PrintStream console) {
		super();
		// Faz a instacia do Twitter - isso é reutilizavel e seguro
		// Conecta no Twitter e passa as autorizações
		this.twitter = TwitterFactory.getSingleton();
		this.consolePrint = console;
		this.status = new ArrayList<Status>();
	}

	/**
	 * Esse método será o que enviará um tweet a partir de uma mensagem recebida
	 * 
	 * @param String a mensagem que deseja enviar
	 * @return String mensagem
	 * @author diego.mendes
	 */
	public String enviarTweet(String mensagem) throws TwitterException, IOException {
		try {
			Status status = twitter.updateStatus(mensagem);
			return status.getText();
		} catch (Exception e) {
			System.out.print(e);
			return null;
		}
	}

	/**
	 * Recebe os 2000 tweets mais recentes de um usuário em particular e seta em um
	 * ArrayList
	 * 
	 * @param String nome do usuário sem o @
	 * @author diego.mendes
	 */
	public List<Tweet> buscaTweets(String username, Integer qtd) throws TwitterException, IOException {
		Paging page = new Paging(1, qtd);
		try {
			page.setPage(1);
			status.addAll(twitter.getHomeTimeline(page));
		} catch (Exception e) {
			System.out.print("Erro na " + getClass().getName() + " no método buscaTweets.");
			
		}

		Date dtPesquisa = Calendar.getInstance().getTime();
		List<Tweet> tweets = new ArrayList<Tweet>();
		for (Status stat : status) {
		
			Tweet tweet = new Tweet();
			tweet.setIdTweet(stat.getId());
			tweet.setCdReplayToStatus(stat.getInReplyToStatusId());
			tweet.setCdReplayToUser(stat.getInReplyToUserId());
			tweet.setTxtUserScreenName(stat.getUser().getScreenName());
			tweet.setTxtTexto(EmojiUtils.shortCodify(stat.getText()));
			tweet.setCdUserId(stat.getUser().getId());
			tweet.setDtDataPostagem(stat.getCreatedAt());
			tweet.setDtPesquisa(dtPesquisa);
			tweet.setTxtNormal(stat.getText());
			tweets.add(tweet);
			
		}

		return tweets;
	}

	/**
	 * pesquisa por uma marcação de perfil '@' ou uma marcação de assunto '#' e seta
	 * em um ArrayList
	 * 
	 * @param String Pesquisa
	 * @author diego.mendes
	 */
	public List<Status> buscaTweetsPorParametros(String parametros) throws TwitterException, IOException {
		HashMap<Long, String> tweets = new HashMap<Long, String>();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.YEAR, -2);

		Query query = new Query();
		query.setCount(2000);
		query.setLang("pt");
		query.setSince(calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-"
				+ calendar.get(Calendar.DAY_OF_MONTH));
		query.setQuery(parametros);
		QueryResult resul = twitter.search(query);
		if (resul.getTweets().size() > 0) {
			status = resul.getTweets();
			for (Status status : resul.getTweets()) {
				tweets.put(status.getId(), status.getText());
			}

			return status;
		}
		return null;
	}

	/**
	 * Enviar mensagem direta para um perfil
	 * 
	 * @param String username, String mensagem
	 * @author diego.mendes
	 * @throws TwitterException
	 */
	public boolean enviarMensagemDireta(String username, String mensagem) throws TwitterException {
		try {
			DirectMessage message = twitter.directMessages().sendDirectMessage(username, mensagem);
			if (message.getText() != null)
				return true;
			else
				return false;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Receber mensagem direta
	 * 
	 * @param String username, String mensagem
	 * @author diego.mendes
	 * @throws TwitterException
	 */
	public void recebeMensagemDireta() throws TwitterException {
		DirectMessageList messages = twitter.directMessages().getDirectMessages(20);
		for (DirectMessage mensage : messages) {
			User user = twitter.showUser(mensage.getSenderId());
			logger.info("Mensagem enviada: " + mensage.getText() + " de: @" + user.getScreenName());
		}
	}

	/**
	 * Responder um tweet público
	 * 
	 * @param Long tweetIdToReplay, String mensagem
	 * @author diego.mendes
	 * @throws TwitterException
	 */
	public boolean responderTweetPublico(Long tweetIdToReplay, String mensagem) throws TwitterException {
		try {

			Status status = twitter.showStatus(tweetIdToReplay);

			StatusUpdate su = new StatusUpdate("@" + status.getUser().getScreenName() + " " + mensagem);
			su.setInReplyToStatusId(status.getId());
			GeoLocation location = null;
			if (status.getGeoLocation() != null)
				location = new GeoLocation(status.getGeoLocation().getLatitude(),
						status.getGeoLocation().getLatitude());
			su.setLocation(location);
			status = twitter.updateStatus(su);
			System.out.print(status);
			return true;
		} catch (Exception e) {
			System.out.print(e);
			return false;
		}
	}

	public Twitter getTwitter() {
		return twitter;
	}

	public void setTwitter(Twitter twitter) {
		this.twitter = twitter;
	}

	public PrintStream getConsolePrint() {
		return consolePrint;
	}

	public void setConsolePrint(PrintStream consolePrint) {
		this.consolePrint = consolePrint;
	}

	public List<Status> getStatus() {
		return status;
	}

	public void setStatus(List<Status> status) {
		this.status = status;
	}

}
