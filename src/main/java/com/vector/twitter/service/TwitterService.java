package com.vector.twitter.service;

import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.vector.twitter.models.Tweet;
import com.vector.twitter.repository.TweetRepository;
import com.vector.twitter.utils.Twitterer;

import twitter4j.Status;
import twitter4j.TwitterException;

@Service
@Component
public class TwitterService {
	/*
	@Autowired
	private TweetRepository tweetRepository;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass()); 
	
	private static PrintStream consolePrint;

	public  String enviarTweet(String msg) throws TwitterException, IOException {
		try {
			Twitterer twitter = new Twitterer(consolePrint);
			return twitter.enviarTweet(msg);
		} catch (TwitterException e) {
			logger.error(e.toString());
			return null;
		}
	}

//	@Scheduled(initialDelay = 0, fixedDelay = 60000)
	private  void enviarTweet() throws TwitterException, IOException {
		try {
			Twitterer twitter = new Twitterer(consolePrint);
			String mensagem = "Olá, teste de API twitter4j!";
			twitter.enviarTweet(mensagem);
		} catch (TwitterException e) {
			logger.error(e.toString());
		}
		
	}
	
	public List<Tweet> buscarUltimosTweets(String user,Integer qtd) throws TwitterException, IOException {
		try {
			if(!user.contains("@"))
				user = "@".concat(user);
			Twitterer twitter = new Twitterer(consolePrint);
			List<Tweet> tweets = twitter.buscaTweets(user, qtd);
			try {
				tweetRepository.saveAll(tweets);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return tweets;
		} catch (TwitterException e) {
			logger.error(e.toString());
			return null;
		}
	}
	
	@Scheduled(initialDelay = 0, fixedDelay = 60000)
	private  void buscarUltimosTweets() throws TwitterException, IOException {
		try {
			Twitterer twitter = new Twitterer(consolePrint);
		//	twitter.buscaTweets("@DiegoAl37526055");
		} catch (Exception e) {
			logger.error(e.toString());
		}
		
	}
	
	public  List <Status>  pesquisaTweets(String parametro) throws TwitterException, IOException {
		try {
			Twitterer twitter = new Twitterer(consolePrint);
			List<Status> tweets = twitter.buscaTweetsPorParametros(parametro);
			return tweets;
		} catch (TwitterException e) {
			logger.error(e.toString());
			return null;
		}
		
	}

	public boolean enviarMensagemDireta(String userTo, String msg) {
		try {
			Twitterer twitter = new Twitterer(consolePrint);
			twitter.enviarMensagemDireta(userTo, msg);
			return true;
		} catch (TwitterException e) {
			logger.error(e.toString());
			return false;
		}
	}
	/*
	//Pensar em uma lógica melhor
	public void receberMensagemDireta() {
//	@Scheduled(initialDelay = 0, fixedDelay = 60000)
	private  void pesquisaTweets() throws TwitterException, IOException {
		try {
			Twitterer twitter = new Twitterer(consolePrint);
			twitter.buscaTweetsPorParametros("Phelipe");
		} catch (TwitterException e) {
			logger.error(e.toString());
		}
		
	}
	
	
//	@Scheduled(initialDelay = 0, fixedDelay = 60000)
//	private void enviarMensagemDireta() {
		try {
		//	Twitterer twitter = new Twitterer(consolePrint);
		//	twitter.enviarMensagemDireta("@phelipex","Olá, eu sou o goku!");
		} catch (TwitterException e) {
			logger.error(e.toString());
		}
	}
	
//	@Scheduled(initialDelay = 0, fixedDelay = 60000)
//	private void receberMensagemDireta() {
		try {
			Twitterer twitter = new Twitterer(consolePrint);
			twitter.recebeMensagemDireta();
		} catch (TwitterException e) {
			logger.error(e.toString());
		}
	}

	public boolean responderTweet(Long idTweet, String msg) throws TwitterException {
		
		Twitterer twitter = new Twitterer(consolePrint);
			Long id = new Long(idTweet);
			if(twitter.responderTweetPublico(id, msg))
				return true;
			else
				return false;
	}
	
	/*
//	@Scheduled(initialDelay = 0, fixedDelay = 60000)
	private void responderTweet() {
		try {
			Twitterer twitter = new Twitterer(consolePrint);
			Long id = new Long("1214966034144333825");
			twitter.responderTweetPublico(id, "Mensagem de resposta!");
		} catch (TwitterException e) {
			logger.error(e.toString());
		}
	}
	*/

	
}
