package com.vector.twitter.models;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.DirectMessage;
import twitter4j.DirectMessageList;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

public class Twitterer {
	private Logger logger = LoggerFactory.getLogger(this.getClass()); 
	
	private Twitter twitter;
	private PrintStream consolePrint;
	private List<Status> status;
	
	public Twitterer(PrintStream console) {
		super();
		//Faz a instacia do Twitter - isso é reutilizavel e seguro
		//Conecta no Twitter e passa as autorizações
		this.twitter = TwitterFactory.getSingleton();
		this.consolePrint = console;
		this.status = new ArrayList<Status>();
	}
	
	/**
	 *	Esse método será o que enviará um tweet a partir de uma mensagem recebida
	 *	@param String a mensagem que deseja enviar
	 *	@author diego.mendes 
	 * */
	public void enviarTweet(String mensagem) throws TwitterException, IOException {
		Status status = twitter.updateStatus(mensagem);
		System.out.print("Status atualizado com sucesso para ("+ status.getText() +")");
	}
	
	/**
	 * Recebe os 2000 tweets mais recentes de um usuário em particular e seta em um ArrayList
	 * @param String nome do usuário sem o @
	 * @author diego.mendes 
	 */
	public void buscaTweets(String username) throws TwitterException, IOException {
		Paging page = new Paging(1, 200);
		
		for(int p = 1; p <= 10; p++) {
			try {
				page.setPage(p);
				status.addAll(twitter.getUserTimeline(username, page));
			} catch (Exception e) {
				System.out.print("Erro na "+getClass().getName()+" no método buscaTweets.");
				break;
			}
		}

		for(int p = 0; p <= status.size()-1; p++) {
			logger.info("Tweet #"+p+": ID:"+status.get(p).getId()+" Mensagem: "+status.get(p).getText());
		}
	}
	
	/**
	 * pesquisa por uma marcação de perfil '@' ou uma marcação de assunto '#' e seta em um ArrayList
	 * @param String Pesquisa
	 * @author diego.mendes 
	 */
	public void buscaTweetsPorParametros(String parametros) throws TwitterException, IOException {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.YEAR, -2);
		
 		Query query = new Query();
		query.setCount(2000);
		query.setLang("pt");
		query.setSince(calendar.get(Calendar.YEAR)+"-"+(calendar.get(Calendar.MONTH)+1)+"-"+calendar.get(Calendar.DAY_OF_MONTH));
		query.setQuery(parametros);
		QueryResult resul = twitter.search(query);
		
		int p = 0;
		for(Status status: resul.getTweets()) {
			logger.info("Tweet #"+p+": ID:"+status.getId()+" Mensagem: "+status.getText());
			p++;
		}
		
	}
	
	/**
	 * Enviar mensagem direta para um perfil
	 * @param String username, String mensagem 
	 * @author diego.mendes 
	 * @throws TwitterException 
	 */
	public void enviarMensagemDireta(String username,String mensagem) throws TwitterException {
		DirectMessage message = twitter.directMessages().sendDirectMessage(username, mensagem);
		logger.info("Mensagem enviada: "+message.getText()+" para: "+message.getRecipientId());
	}
	
	/**
	 * Receber mensagem direta 
	 * @param String username, String mensagem 
	 * @author diego.mendes 
	 * @throws TwitterException 
	 */
	public void recebeMensagemDireta() throws TwitterException {
		DirectMessageList messages = twitter.directMessages().getDirectMessages(20);
		for(DirectMessage mensage: messages) {
			User user = twitter.showUser(mensage.getSenderId());
			logger.info("Mensagem enviada: "+mensage.getText()+" de: @"+user.getScreenName());
		}
	}
	
	/**
	 * Responder um tweet público
	 * @param Long tweetIdToReplay, String mensagem 
	 * @author diego.mendes 
	 * @throws TwitterException 
	 */
	public void responderTweetPublico(Long tweetIdToReplay, String mensagem) throws TwitterException {
		StatusUpdate replay = new StatusUpdate(mensagem);
		replay.inReplyToStatusId(tweetIdToReplay);
		Status status = twitter.updateStatus(replay);
		logger.info("Replay enviado: "+status.getText()+" para o id de mensagem: "+status.getInReplyToStatusId());
	}
	
	public Twitter getTwitter() {
		return twitter;
	}
	public void setTwitter(Twitter twitter) {
		this.twitter = twitter;
	}
	public PrintStream getConsolePrint() {
		return consolePrint;
	}
	public void setConsolePrint(PrintStream consolePrint) {
		this.consolePrint = consolePrint;
	}
	public List<Status> getStatus() {
		return status;
	}
	public void setStatus(List<Status> status) {
		this.status = status;
	}

}
