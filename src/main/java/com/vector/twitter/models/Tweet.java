package com.vector.twitter.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(schema = "redes_sociais", name = "tb_tweet")
public class Tweet {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cd_tweet")
	private Long cdTweet;
	
	@Column(name = "id_tweet")
	private Long idTweet;
	
	@Column(name = "txt_texto")
	private String txtTexto;
	
	@Column(name = "cd_replay_to_status")
	private Long cdReplayToStatus;
	
	@Column(name = "cd_replay_to_user")
	private Long cdReplayToUser;
	
	@Column(name = "cd_user_id")
	private Long cdUserId;
	
	@Column(name = "txt_user_screen_name")
	private String txtUserScreenName;
	
	@Column(name = "dt_data_postagem")
	private Date dtDataPostagem; 
	
	@Column(name = "cd_atendimento")
	private Long cdAtendimento;
	
	@Column(name = "dt_pesquisa")
	private Date dtPesquisa; 
	
	@Transient
	private String txtNormal;
	
	public Tweet() {
		super();
	}

	public Tweet(Long cdTweet, String txtTexto, Long cdReplayToStatus, Long cdReplayToUser, Long cdUserId,
			String txtUserScreenName, Date dtDataPostagem, Long cdAtendimento, Date dtPesquisa) {
		super();
		this.cdTweet = cdTweet;
		this.txtTexto = txtTexto;
		this.cdReplayToStatus = cdReplayToStatus;
		this.cdReplayToUser = cdReplayToUser;
		this.cdUserId = cdUserId;
		this.txtUserScreenName = txtUserScreenName;
		this.dtDataPostagem = dtDataPostagem;
		this.cdAtendimento = cdAtendimento;
		this.dtPesquisa = dtPesquisa;
	}

	public Long getCdTweet() {
		return cdTweet;
	}

	public void setCdTweet(Long cdTweet) {
		this.cdTweet = cdTweet;
	}

	public String getTxtTexto() {
		return txtTexto;
	}

	public void setTxtTexto(String txtTexto) {
		this.txtTexto = txtTexto;
	}

	public Long getCdReplayToStatus() {
		return cdReplayToStatus;
	}

	public void setCdReplayToStatus(Long cdReplayToStatus) {
		this.cdReplayToStatus = cdReplayToStatus;
	}

	public Long getCdReplayToUser() {
		return cdReplayToUser;
	}

	public void setCdReplayToUser(Long cdReplayToUser) {
		this.cdReplayToUser = cdReplayToUser;
	}

	public Long getCdUserId() {
		return cdUserId;
	}

	public void setCdUserId(Long cdUserId) {
		this.cdUserId = cdUserId;
	}

	public String getTxtUserScreenName() {
		return txtUserScreenName;
	}

	public void setTxtUserScreenName(String txtUserScreenName) {
		this.txtUserScreenName = txtUserScreenName;
	}

	public Date getDtDataPostagem() {
		return dtDataPostagem;
	}

	public void setDtDataPostagem(Date dtDataPostagem) {
		this.dtDataPostagem = dtDataPostagem;
	}

	public Long getCdAtendimento() {
		return cdAtendimento;
	}

	public void setCdAtendimento(Long cdAtendimento) {
		this.cdAtendimento = cdAtendimento;
	}

	public Date getDtPesquisa() {
		return dtPesquisa;
	}

	public void setDtPesquisa(Date dtPesquisa) {
		this.dtPesquisa = dtPesquisa;
	}

	public String getTxtNormal() {
		return txtNormal;
	}

	public void setTxtNormal(String txtNormal) {
		this.txtNormal = txtNormal;
	}

	public Long getIdTweet() {
		return idTweet;
	}

	public void setIdTweet(Long idTweet) {
		this.idTweet = idTweet;
	}
	
} 