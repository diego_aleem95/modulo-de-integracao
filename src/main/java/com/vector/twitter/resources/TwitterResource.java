package com.vector.twitter.resources;

import java.io.IOException;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vector.twitter.models.Tweet;
import com.vector.twitter.service.TwitterService;

import twitter4j.Status;
import twitter4j.TwitterException;

@RestController
@RequestMapping("/twitter")
public class TwitterResource {
/*
	@Autowired
	private TwitterService twitterService;

	@PostMapping
	@RequestMapping("/enviarTweet")
	public ResponseEntity<?> enviarTweet(@RequestBody String msg) throws TwitterException, IOException {
		String retorno = twitterService.enviarTweet(msg);
		if (retorno != null)
			return ResponseEntity.ok(retorno);
		else
			return ResponseEntity.badRequest().build();
	}

	@GetMapping
	@RequestMapping("/timeLinePerfil")
	public ResponseEntity<?> buscarUltimosTweets(@RequestParam String user, @RequestParam Integer qtd)
			throws TwitterException, IOException {
		List<Tweet> retorno = twitterService.buscarUltimosTweets(user, qtd);
		if (retorno != null)
			return ResponseEntity.ok(retorno);
		else
			return ResponseEntity.badRequest().build();
	}

	@GetMapping
	@RequestMapping("/pesquisaTweets")
	public ResponseEntity<?> pesquisaTweets(@RequestParam String parametros) throws TwitterException, IOException {
		List<Status> retorno = twitterService.pesquisaTweets(parametros);
		if (retorno != null)
			return ResponseEntity.ok(retorno);
		else
			return ResponseEntity.badRequest().build();
	}

	@PostMapping
	@RequestMapping("/enviarMensagemDireta")
	public ResponseEntity<?> enviarMensagemDireta(@RequestBody String userTo, @RequestBody String msg)
			throws TwitterException, IOException {
		boolean retorno = twitterService.enviarMensagemDireta(userTo, msg);
		if (retorno)
			return ResponseEntity.ok().build();
		else
			return ResponseEntity.badRequest().build();
	}

	@PostMapping("/responderTweet")
	public ResponseEntity<?> responderTweet(@RequestParam(name = "id") Long id, String screen, @RequestParam(name = "msg") String msg)
			throws TwitterException, IOException {
		 
		boolean retorno = twitterService.responderTweet(id, msg);
		 if(retorno)
			 return ResponseEntity.ok().build();
		 else
			 return ResponseEntity.badRequest().build();
		 
		
	}
	*/
}
