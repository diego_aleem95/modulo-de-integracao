package com.vector.integracoes;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan(basePackages = "com.vector.twitter.service")
public class IntegracoesApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntegracoesApplication.class, args);
	}
	
	@Bean
	public DataSource dataSource() 
	{
	  JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
	  DataSource dataSource = dataSourceLookup.getDataSource("java:jboss/datasources/SISCRMHOMOLOG");
	  return dataSource;
	}

}
