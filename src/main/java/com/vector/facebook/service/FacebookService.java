package com.vector.facebook.service;

import java.io.PrintStream;
import java.net.MalformedURLException;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.vector.facebook.model.Facebooker;

import facebook4j.FacebookException;


@Service
@Component
public class FacebookService {
	
	private static PrintStream consolePrint;
	
	public void test() throws FacebookException, MalformedURLException {
		Facebooker face = new Facebooker(consolePrint);
		face.publicarPost("Teste de post");
	}

}
