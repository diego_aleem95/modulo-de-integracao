package com.vector.facebook.model;

import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import facebook4j.Account;
import facebook4j.Conversation;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.Post;
import facebook4j.PostUpdate;

public class Facebooker {

	// private Logger logger = LoggerFactory.getLogger(this.getClass());

	private Facebook facebook;
	private PrintStream consolePrint;
	private List<Post> posts;

	public Facebooker(PrintStream consolePrint) throws FacebookException {
		super();
		this.facebook = FacebookFactory.getSingleton();
		this.consolePrint = consolePrint;
		this.posts = new ArrayList<Post>();
		this.facebook.setOAuthAccessToken(this.facebook.extendTokenExpiration());
		this.facebook.setOAuthPermissions(
				"email, manage_pages, pages_show_list, pages_messaging, pages_messaging_phone_number, pages_messaging_subscriptions, public_profile");
	}

	public void publicarPost(String msg) throws FacebookException, MalformedURLException {
		List<Account> accounts = facebook.getAccounts();
		Account yourPageAccount = accounts.get(0);
		String pageAccessToken = yourPageAccount.getAccessToken();
		List<Conversation> con = facebook.getConversations();
		System.out.println(con.get(0).getSenders());
	}

	public Facebooker() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Facebook getFacebook() {
		return facebook;
	}

	public void setFacebook(Facebook facebook) {
		this.facebook = facebook;
	}

	public PrintStream getConsolePrint() {
		return consolePrint;
	}

	public void setConsolePrint(PrintStream consolePrint) {
		this.consolePrint = consolePrint;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

}
