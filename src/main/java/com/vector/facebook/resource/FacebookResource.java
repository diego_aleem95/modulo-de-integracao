package com.vector.facebook.resource;

import java.net.MalformedURLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vector.facebook.service.FacebookService;

import facebook4j.FacebookException;


@RestController
@RequestMapping("/facebook")
public class FacebookResource {
	
	@Autowired
	private FacebookService facebookService;
	
	@GetMapping("/teste")
	public void teste() throws FacebookException, MalformedURLException {
		facebookService.test();
	}
}
