package com.vector.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_falta_grave", schema="monitoria")
public class FaltaGrave implements Serializable{
	
	private static final long serialVersionUID = 2733540222582626660L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="cd_falta_grave")
	private Long cdFaltaGrave;
	
	@Column(name="ds_falta_grave")
	private String dsFaltaGrave;

	public Long getCdFaltaGrave() {
		return cdFaltaGrave;
	}

	public void setCdFaltaGrave(Long cdFaltaGrave) {
		this.cdFaltaGrave = cdFaltaGrave;
	}

	public String getDsFaltaGrave() {
		return dsFaltaGrave;
	}

	public void setDsFaltaGrave(String dsFaltaGrave) {
		this.dsFaltaGrave = dsFaltaGrave;
	}
	

}
