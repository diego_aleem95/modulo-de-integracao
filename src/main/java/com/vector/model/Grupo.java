package com.vector.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="tb_grupo", schema="crm")
public class Grupo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="cd_grupo")
	private Long cdGrupo;
	@Column(name="cd_perfil")
	private Long cdPerfil;
	@Column(name="nm_grupo")
	private String nmGrupo;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="tr_funcionalidade_grupo", schema="crm", joinColumns= @JoinColumn(name="cd_grupo")
	,inverseJoinColumns = @JoinColumn(name="cd_funcionalidade"))
	private List<Funcionalidade> funcionalidades;
	public Long getCdGrupo() {
		return cdGrupo;
	}
	public void setCdGrupo(Long cdGrupo) {
		this.cdGrupo = cdGrupo;
	}
	public Long getCdPerfil() {
		return cdPerfil;
	}
	public void setCdPerfil(Long cdPerfil) {
		this.cdPerfil = cdPerfil;
	}
	public String getNmGrupo() {
		return nmGrupo;
	}
	public void setNmGrupo(String nmGrupo) {
		this.nmGrupo = nmGrupo;
	}
	public List<Funcionalidade> getFuncionalidades() {
		return funcionalidades;
	}
	public void setFuncionalidades(List<Funcionalidade> funcionalidades) {
		this.funcionalidades = funcionalidades;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdGrupo == null) ? 0 : cdGrupo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grupo other = (Grupo) obj;
		if (cdGrupo == null) {
			if (other.cdGrupo != null)
				return false;
		} else if (!cdGrupo.equals(other.cdGrupo))
			return false;
		return true;
	}
	
	
}
