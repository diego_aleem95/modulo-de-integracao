package com.vector.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tb_formulario", schema="monitoria")
public class Formulario  implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="cd_formulario")
	private Long cdFormulario;
	
	@Column(name="ds_formulario")
	private String dsFormulario;
	
	@Column(name="dt_criacao")
	private Date dtCriacao;
	
	@Column(name="status")
	private Boolean status;
	
	@Column(name="cd_formula")
	private Long cdFormula;
	
	@Column(name="cd_usuario_criacao")
	private Long cdUsuarioCriacao;
	/*
	@Column(name="erro_grave")
	private Boolean grave;
	*/
	@ManyToOne
	@JoinColumn(name = "cd_tema", referencedColumnName = "cd_tema")
	private Tema tema;
	
	@OneToMany(mappedBy="cdFormulario")
	private List<Pergunta> perguntas = new ArrayList<>();
/*
	public Boolean isGrave() {
		return grave;
	}

	public void setGrave(Boolean grave) {
		this.grave = grave;
	}
*/
	public Long getCdFormulario() {
		return cdFormulario;
	}

	public Long getCdFormula() {
		return cdFormula;
	}

	public void setCdFormula(Long cdFormula) {
		this.cdFormula = cdFormula;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public void setCdFormulario(Long cdFormulario) {
		this.cdFormulario = cdFormulario;
	}

	public String getDsFormulario() {
		return dsFormulario;
	}
	
	public Long getCdUsuarioCriacao() {
		return cdUsuarioCriacao;
	}

	public void setCdUsuarioCriacao(Long cdUsuarioCriacao) {
		this.cdUsuarioCriacao = cdUsuarioCriacao;
	}

	public void setDsFormulario(String dsFormulario) {
		this.dsFormulario = dsFormulario;
	}

	public Date getDtCriacao() {
		return dtCriacao;
	}

	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	public Tema getTema() {
		return tema;
	}

	public void setTema(Tema tema) {
		this.tema = tema;
	}
	
	public List<Pergunta> getPerguntas() {
		return perguntas;
	}

	public void setPerguntas(List<Pergunta> perguntas) {
		this.perguntas = perguntas;
	}


	
	public Formulario(Long cdFormulario, String dsFormulario, Date dtCriacao,Long cdUsuarioCriacao, Tema tema,
			List<Pergunta> perguntas) {
		super();
		this.cdFormulario = cdFormulario;
		this.dsFormulario = dsFormulario;
		this.dtCriacao = dtCriacao;
		this.cdUsuarioCriacao = cdUsuarioCriacao;
		this.tema = tema;
		this.perguntas = perguntas;
	}

	public Formulario() {}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdFormulario == null) ? 0 : cdFormulario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Formulario other = (Formulario) obj;
		if (cdFormulario == null) {
			if (other.cdFormulario != null)
				return false;
		} else if (!cdFormulario.equals(other.cdFormulario))
			return false;
		return true;
	}
	
	
}
