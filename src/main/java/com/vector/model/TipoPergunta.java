package com.vector.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="monitoria", name="tb_tipo_pergunta")
public class TipoPergunta {
	
	@Id
	@Column(name="cd_tipo_pergunta")
	private Long cdTipoPergunta;
	@Column(name="ds_tipo_pergunta")
	private String dsTipoPergunta;
	public Long getCdTipoPergunta() {
		return cdTipoPergunta;
	}
	public void setCdTipoPergunta(Long cdTipoPergunta) {
		this.cdTipoPergunta = cdTipoPergunta;
	}
	public String getDsTipoPergunta() {
		return dsTipoPergunta;
	}
	public void setDsTipoPergunta(String dsTipopergunta) {
		this.dsTipoPergunta = dsTipopergunta;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdTipoPergunta == null) ? 0 : cdTipoPergunta.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoPergunta other = (TipoPergunta) obj;
		if (cdTipoPergunta == null) {
			if (other.cdTipoPergunta != null)
				return false;
		} else if (!cdTipoPergunta.equals(other.cdTipoPergunta))
			return false;
		return true;
	}
	public TipoPergunta(Long cdTipoPergunta, String dsTipopergunta) {
		super();
		this.cdTipoPergunta = cdTipoPergunta;
		this.dsTipoPergunta = dsTipopergunta;
	}
	
	public TipoPergunta() {}
	
}
