package com.vector.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_alternativa", schema="monitoria")
public class Alternativa implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="cd_alternativa")
	private Long cdAlternativa;

	@Column(name="ds_alternativa")
	private String dsAlternativa;
	
	
	public Alternativa() {}
	
	
	public Alternativa(Long cdAlternativa, String dsAlternativa) {
		this.cdAlternativa = cdAlternativa;
		this.dsAlternativa = dsAlternativa;
		
		
	}


	public Long getCdAlternativa() {
		return cdAlternativa;
	}

	public void setCdAlternativa(Long cdAlternativa) {
		this.cdAlternativa = cdAlternativa;
	}

	public String getDsAlternativa() {
		return dsAlternativa;
	}

	public void setDsAlternativa(String dsAlternativa) {
		this.dsAlternativa = dsAlternativa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdAlternativa == null) ? 0 : cdAlternativa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alternativa other = (Alternativa) obj;
		if (cdAlternativa == null) {
			if (other.cdAlternativa != null)
				return false;
		} else if (!cdAlternativa.equals(other.cdAlternativa))
			return false;
		return true;
	}
	
	
}
