package com.vector.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_tipo_campo", schema="monitoria")
public class TipoCampo implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="cd_tipo_campo")
	private Long cdTipoCampo;
	
	@Column(name="ds_campo")
	private String dsCampo;

	public Long getCdTipoCampo() {
		return cdTipoCampo;
	}

	public void setCdTipoCampo(Long cdTipoCampo) {
		this.cdTipoCampo = cdTipoCampo;
	}

	public String getDsCampo() {
		return dsCampo;
	}

	public void setDsCampo(String dsCampo) {
		this.dsCampo = dsCampo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdTipoCampo == null) ? 0 : cdTipoCampo.hashCode());
		result = prime * result + ((dsCampo == null) ? 0 : dsCampo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoCampo other = (TipoCampo) obj;
		if (cdTipoCampo == null) {
			if (other.cdTipoCampo != null)
				return false;
		} else if (!cdTipoCampo.equals(other.cdTipoCampo))
			return false;
		if (dsCampo == null) {
			if (other.dsCampo != null)
				return false;
		} else if (!dsCampo.equals(other.dsCampo))
			return false;
		return true;
	}
	
	
		
}
