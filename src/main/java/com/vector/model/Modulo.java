package com.vector.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_modulo", schema="crm")
public class Modulo implements Serializable{

	private static final long serialVersionUID = -8886632531853786688L;

	@Id
	@Column(name="cd_modulo")
	private int cdModulo;
	
	@Column(name="nm_modulo")
	private String nomeModulo;
	
	@Column(name="in_modulo")
	private char statusModulo;
	
	@Column(name="in_documento")
	private char statusDocumento;

	public int getCdModulo() {
		return cdModulo;
	}

	public void setCdModulo(int cdModulo) {
		this.cdModulo = cdModulo;
	}

	public String getNomeModulo() {
		return nomeModulo;
	}

	public void setNomeModulo(String nomeModulo) {
		this.nomeModulo = nomeModulo;
	}

	public char getStatusModulo() {
		return statusModulo;
	}

	public void setStatusModulo(char statusModulo) {
		this.statusModulo = statusModulo;
	}

	public char getStatusDocumento() {
		return statusDocumento;
	}

	public void setStatusDocumento(char statusDocumento) {
		this.statusDocumento = statusDocumento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cdModulo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Modulo other = (Modulo) obj;
		if (cdModulo != other.cdModulo)
			return false;
		return true;
	}
	
	public Modulo() {}

	public Modulo(int cdModulo, String nomeModulo, char statusModulo, char statusDocumento) {
		super();
		this.cdModulo = cdModulo;
		this.nomeModulo = nomeModulo;
		this.statusModulo = statusModulo;
		this.statusDocumento = statusDocumento;
	}

	
	
}
