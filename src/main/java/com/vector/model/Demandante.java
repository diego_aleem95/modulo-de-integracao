package com.vector.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_demandante", schema="crm")
public class Demandante implements Serializable {
	
	private static final long serialVersionUID = -5626382545519412469L;

	@Id
	@Column(name="cd_demandante")
	private Long cdDemandante;
	
	@Column(name="cd_cidade")
	private Integer cdCidade;
	
	@Column(name="cd_usuario")
	private Integer cdUsuario;
	
	@Column(name="cd_tipo_demandante")
	private int cdTipoDemandante;
	
	@Column(name="nu_cpf")
	private String cpf;

	@Column(name="nu_cnpj")
	private String cnpj;
	
	@Column(name="nu_nis")
	private String nis;
	
	@Column(name="nm_demandante")
	private String nomeDemandante;
	
	@Column(name="nm_fantasia")
	private String nomeFantasia;
	
	@Column(name="nm_razao_social")
	private String razaoSocial;
	
	@Column(name="ds_email")
	private String email;
	
	@Column(name="in_anonimo")
	private char anonimo;

	public Long getCdDemandante() {
		return cdDemandante;
	}

	public void setCdDemandante(Long cdDemandante) {
		this.cdDemandante = cdDemandante;
	}

	public Integer getCdCidade() {
		return cdCidade;
	}

	public void setCdCidade(Integer cdCidade) {
		this.cdCidade = cdCidade;
	}

	public Integer getCdUsuario() {
		return cdUsuario;
	}

	public void setCdUsuario(Integer cdUsuario) {
		this.cdUsuario = cdUsuario;
	}

	public int getCdTipoDemandante() {
		return cdTipoDemandante;
	}

	public void setCdTipoDemandante(int cdTipoDemandante) {
		this.cdTipoDemandante = cdTipoDemandante;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNis() {
		return nis;
	}

	public void setNis(String nis) {
		this.nis = nis;
	}

	public String getNomeDemandante() {
		return nomeDemandante;
	}

	public void setNomeDemandante(String nomeDemandante) {
		this.nomeDemandante = nomeDemandante;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public char getAnonimo() {
		return anonimo;
	}

	public void setAnonimo(char anonimo) {
		this.anonimo = anonimo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdDemandante == null) ? 0 : cdDemandante.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Demandante other = (Demandante) obj;
		if (cdDemandante == null) {
			if (other.cdDemandante != null)
				return false;
		} else if (!cdDemandante.equals(other.cdDemandante))
			return false;
		return true;
	}

	public Demandante(Long cdDemandante, Integer cdCidade, Integer cdUsuario, int cdTipoDemandante, String cpf, String cnpj,
			String nis, String nomeDemandante, String nomeFantasia, String razaoSocial, String email, char anonimo) {
		super();
		this.cdDemandante = cdDemandante;
		this.cdCidade = cdCidade;
		this.cdUsuario = cdUsuario;
		this.cdTipoDemandante = cdTipoDemandante;
		this.cpf = cpf;
		this.cnpj = cnpj;
		this.nis = nis;
		this.nomeDemandante = nomeDemandante;
		this.nomeFantasia = nomeFantasia;
		this.razaoSocial = razaoSocial;
		this.email = email;
		this.anonimo = anonimo;
	}
	
	
	public Demandante() {}
	
	
	
}
