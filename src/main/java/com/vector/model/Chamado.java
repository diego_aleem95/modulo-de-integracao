package com.vector.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_chamado", schema="crm")
public class Chamado {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="cd_chamado")
	private Long cdChamado;
	
	
	@Column(name="in_manual")
	private int inManual;
	
	@Column(name="nu_protocolo_chamado")
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private String protocolo;
	
	@Column(name="nu_ddd_origem")
	private String ddd;
	
	@Column(name="nu_telefone_origem")
	private String telefone;
	
	@Column(name="dt_fim_ligacao")
	private Date fimLigacao;
	
	@Column(name="dt_inicio_ligacao")
	private Date inicioLigacao;

	@Column(name="ucid")
	private String ucid;
	
	@Column(name="cd_ramal")
	private Integer ramal;
	
	@Column(name="cd_servico")
	private Long servico;
	
	@Column(name="cd_forma_contato")
	private Long cdFormaContato;
	
	@Column(name="ds_gravacao")
	private String dsGravacao;
	

	public Long getCdChamado() {
		return cdChamado;
	}

	public void setCdChamado(Long cdChamado) {
		this.cdChamado = cdChamado;
	}
	
	public Long getCdFormaContato() {
		return cdFormaContato;
	}

	public void setCdFormaContato(Long cdFormaContato) {
		this.cdFormaContato = cdFormaContato;
	}

	public int getInManual() {
		return inManual;
	}

	public void setInManual(int inManual) {
		this.inManual = inManual;
	}

	public String getProtocolo() {
		return protocolo;
	}

	public void setProtocolo(String protocolo) {
		this.protocolo = protocolo;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Date getFimLigacao() {
		return fimLigacao;
	}

	public void setFimLigacao(Timestamp fimLigacao) {
		this.fimLigacao = fimLigacao;
	}

	public Date getInicioLigacao() {
		return inicioLigacao;
	}

	public void setInicioLigacao(Date date) {
		this.inicioLigacao = date;
	}

	public String getUcid() {
		return ucid;
	}

	public void setUcid(String ucid) {
		this.ucid = ucid;
	}

	public Integer getRamal() {
		return ramal;
	}

	public void setRamal(Integer ramal) {
		this.ramal = ramal;
	}

	public Long getServico() {
		return servico;
	}

	public void setServico(Long servico) {
		this.servico = servico;
	}

	public String getDsGravacao() {
		return dsGravacao;
	}

	public void setDsGravacao(String dsGravacao) {
		this.dsGravacao = dsGravacao;
	}	
	

}
