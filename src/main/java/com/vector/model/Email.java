package com.vector.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="crm", name="tb_email")
public class Email {
	
		@Id
		@Column(name="cd_email")
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Long   cdEmail;
		@Column(name="ds_remetente_nome")
		private	String nmRementente;
		@Column(name="ds_mensagem")
		private String dsEmail;
		@Column(name="ds_remetente_email")
		private String emailRemetente;
		@Column(name="ds_destinatario_email")
		private String emailDestinatario;
		@Column(name="dt_recebimento")
		private Date dtRecebimento;
		@Column(name="dt_envio")
		private Date dtEnvio;
		@Column(name="ds_assunto")
		private String dsAssunto;
		
		public String getDsAssunto() {
			return dsAssunto;
		}
		public void setDsAssunto(String dsAssunto) {
			this.dsAssunto = dsAssunto;
		}
		public String getNmRementente() {
			return nmRementente;
		}
		public void setNmRementente(String nmRementente) {
			this.nmRementente = nmRementente;
		}
		public String getDsEmail() {
			return dsEmail;
		}
		public void setDsEmail(String dsEmail) {
			this.dsEmail = dsEmail;
		}
		public String getEmailRemetente() {
			return emailRemetente;
		}
		public void setEmailRemetente(String emailRemetente) {
			this.emailRemetente = emailRemetente;
		}
		public String getEmailDestinatario() {
			return emailDestinatario;
		}
		public void setEmailDestinatario(String emailDestinatario) {
			this.emailDestinatario = emailDestinatario;
		}
		public Date getDtRecebimento() {
			return dtRecebimento;
		}
		public void setDtRecebimento(Date dtRecebimento) {
			this.dtRecebimento = dtRecebimento;
		}
		public Date getDtEnvio() {
			return dtEnvio;
		}
		public void setDtEnvio(Date dtEnvio) {
			this.dtEnvio = dtEnvio;
		}
				
}

