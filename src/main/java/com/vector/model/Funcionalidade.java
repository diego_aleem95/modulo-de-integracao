package com.vector.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_funcionalidade", schema="crm")
public class Funcionalidade implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="cd_funcionalidade")
	private Long cdFuncionalidade;
	
	@Column(name="ds_funcionalidade")
	private String dsFuncionalidade;
	
	public Long getCdFuncionalidade() {
		return cdFuncionalidade;
	}
	public void setCdFuncionalidade(Long cdFuncionalidade) {
		this.cdFuncionalidade = cdFuncionalidade;
	}
	public String getDsFuncionalidade() {
		return dsFuncionalidade;
	}
	public void setDsFuncionalidade(String dsFuncionalidade) {
		this.dsFuncionalidade = dsFuncionalidade;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdFuncionalidade == null) ? 0 : cdFuncionalidade.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionalidade other = (Funcionalidade) obj;
		if (cdFuncionalidade == null) {
			if (other.cdFuncionalidade != null)
				return false;
		} else if (!cdFuncionalidade.equals(other.cdFuncionalidade))
			return false;
		return true;
	}
	
	
}
