package com.vector.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="tb_pergunta", schema="monitoria")
public class Pergunta implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public static final boolean ERRO_CRITICO = true;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="cd_pergunta")
	private Long cdPergunta;
	
	@Column(name="ds_pergunta")
	private String dsPegunta;
	
	@Column(name="erro_critico")
	private boolean erroCritico;
	
	@Column(name="valor")
	private Double valor;
	
	@Column(name="peso")
	private Integer peso;
	
	@Column(name="cd_formulario")
	private Long cdFormulario;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cd_tipo_campo", referencedColumnName = "cd_tipo_campo")
	private TipoCampo tipoCampo;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cd_pergunta_pai", referencedColumnName = "cd_pergunta")
	private Pergunta perguntaPai;
	
	@OneToMany(mappedBy="perguntaPai")
	private List<Pergunta> subItem;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="cd_tipo_pergunta")
	private TipoPergunta tipoPergunta;
	


	@OneToMany(mappedBy="cdPergunta")
	private List<AlternativaPergunta> alternativas = new ArrayList<>();

	public List<Pergunta> getSubItem() {
		return subItem;
	}

	public void setSubItem(List<Pergunta> perguntas) {
		this.subItem = perguntas;
	}
	
	public List<AlternativaPergunta> getAlternativas() {
		return alternativas;
	}

	public void setAlternativas(List<AlternativaPergunta> alternativas) {
		this.alternativas = alternativas;
	}

	public TipoPergunta getTipoPergunta() {
		return this.tipoPergunta;
	}
	
	public void setTipoPergunta(TipoPergunta tp) {
		this.tipoPergunta = tp;
	}
	
	public Long getCdPergunta() {
		return cdPergunta;
	}

	public void setCdPergunta(Long cdPergunta) {
		this.cdPergunta = cdPergunta;
	}

	public String getDsPergunta() {
		return dsPegunta;
	}

	public void setDsPergunta(String dsValor) {
		this.dsPegunta = dsValor;
	}

	public boolean isErroCritico() {
		return erroCritico;
	}

	public void setErroCritico(Boolean erroCritico) {
		this.erroCritico = erroCritico;
	}

	public Long getCdFormulario() {
		return cdFormulario;
	}

	public void setCdFormulario(Long cdFormulario) {
		this.cdFormulario = cdFormulario;
	}

	public TipoCampo getTipoCampo() {
		return tipoCampo;
	}

	public void setTipoCampo(TipoCampo tipoCampo) {
		this.tipoCampo = tipoCampo;
	}

	public Pergunta getPerguntaPai() {
		return perguntaPai;
	}

	public void setPerguntaPai(Pergunta perguntaPai) {
		this.perguntaPai = perguntaPai;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdPergunta == null) ? 0 : cdPergunta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pergunta other = (Pergunta) obj;
		if (cdPergunta == null) {
			if (other.cdPergunta != null)
				return false;
		} else if (!cdPergunta.equals(other.cdPergunta))
			return false;
		return true;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Pergunta(Long cdPergunta, String dsPegunta, boolean erroCritico, Double valor, Integer peso,
			Long cdFormulario, TipoCampo tipoCampo, Pergunta perguntaPai, TipoPergunta tp) {
		super();
		this.cdPergunta = cdPergunta;
		this.dsPegunta = dsPegunta;
		this.erroCritico = erroCritico;
		this.valor = valor;
		this.peso = peso;
		this.cdFormulario = cdFormulario;
		this.tipoCampo = tipoCampo;
		this.perguntaPai = perguntaPai;
		this.tipoPergunta = tp;
		
	}

	public Integer getPeso() {
		return peso;
	}

	public void setPeso(Integer peso) {
		this.peso = peso;
	}

	public Pergunta() {}
	
	
	
}
