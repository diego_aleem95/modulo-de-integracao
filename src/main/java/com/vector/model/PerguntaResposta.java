package com.vector.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tr_pergunta_resposta", schema="monitoria")
public class PerguntaResposta implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="cd_pergunta_resposta")
	private Long cdPerguntaResposta;
	
	@Column(name="dt_resposta")
	private Date dataResposta;
	
	@Column(name="ds_resposta_txt")
	private String dsRespostaTxt;
	
	@Column(name="cd_alternativa")
	private Long cdAlternativa;
	
	@Column(name="cd_pergunta")
	private Long cdPergunta;
	
	@Column(name="cd_avaliacao")
	private Long cdAvaliacao;
	
	public Long getCdAlternativa() {
		return cdAlternativa;
	}

	public void setCdAlternativa(Long cdAlternativa) {
		this.cdAlternativa = cdAlternativa;
	}

	public Long getCdPerguntaResposta() {
		return cdPerguntaResposta;
	}

	public void setCdPerguntaResposta(Long cdPerguntaResposta) {
		this.cdPerguntaResposta = cdPerguntaResposta;
	}

	public Date getDataResposta() {
		return dataResposta;
	}

	public void setDataResposta(Date dataResposta) {
		this.dataResposta = dataResposta;
	}

	public String getDsRespostaTxt() {
		return dsRespostaTxt;
	}

	public void setDsRespostaTxt(String dsResposta) {
		this.dsRespostaTxt = dsResposta;
	}

	public Long getCdPergunta() {
		return cdPergunta;
	}

	public void setCdPergunta(Long cdPergunta) {
		this.cdPergunta = cdPergunta;
	}

	public Long getCdAvaliacao() {
		return cdAvaliacao;
	}

	public void setcDAvaliacao(Long cdAvaliacao) {
		this.cdAvaliacao = cdAvaliacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdPerguntaResposta == null) ? 0 : cdPerguntaResposta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PerguntaResposta other = (PerguntaResposta) obj;
		if (cdPerguntaResposta == null) {
			if (other.cdPerguntaResposta != null)
				return false;
		} else if (!cdPerguntaResposta.equals(other.cdPerguntaResposta))
			return false;
		return true;
	}

	public PerguntaResposta(Long cdPerguntaResposta, Date dataResposta, String dsResposta, 
			Long cdPergunta, Long cdAvaliacao) {
		this.cdPerguntaResposta = cdPerguntaResposta;
		this.dataResposta = dataResposta;
		this.dsRespostaTxt = dsResposta;
		this.cdPergunta = cdPergunta;
		this.cdAvaliacao = cdAvaliacao;
	}
	
	public PerguntaResposta() {}
	
	
}
