package com.vector.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = "telefonia", name = "cdr")
public class Cdr implements Serializable{
	
	private static final long serialVersionUID = -1504762777690024131L;
	
	@EmbeddedId
	private CdrPK id;
	
	@Column(name = "calldate")
	private Date calldate;
	@Column(name = "clid")
	private String clid;
	@Column(name = "src")
	private String  src;
	@Column(name = "dst")
	private String dst;
	@Column(name = "dcontext")
	private String dcontext;
	@Column(name = "dstchannel")
	private String dstchannel;
	@Column(name = "lastapp")
	private String lastapp;
	@Column(name = "lastdata")
	private String lastdata;
	@Column(name = "duration")
	private BigInteger duration;
	@Column(name = "disposition")
	private String disposition;
	@Column(name = "amaflags")
	private BigInteger amaflags;
	@Column(name = "accountcode")
	private String accountcode;
	@Column(name = "uniqueid")
	private String uniqueid;
	@Column(name = "userfield")
	private String gravacao;
	
		
	public CdrPK getId() {
		return id;
	}
	public void setId(CdrPK id) {
		this.id = id;
	}
	public Date getCalldate() {
		return calldate;
	}
	public void setCalldate(Date calldate) {
		this.calldate = calldate;
	}
	public String getClid() {
		return clid;
	}
	public void setClid(String clid) {
		this.clid = clid;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	public String getDst() {
		return dst;
	}
	public void setDst(String dst) {
		this.dst = dst;
	}
	public String getDcontext() {
		return dcontext;
	}
	public void setDcontext(String dcontext) {
		this.dcontext = dcontext;
	}
	public String getDstchannel() {
		return dstchannel;
	}
	public void setDstchannel(String dstchannel) {
		this.dstchannel = dstchannel;
	}
	public String getLastapp() {
		return lastapp;
	}
	public void setLastapp(String lastapp) {
		this.lastapp = lastapp;
	}
	public String getLastdata() {
		return lastdata;
	}
	public void setLastdata(String lastdata) {
		this.lastdata = lastdata;
	}
	public BigInteger getDuration() {
		return duration;
	}
	public void setDuration(BigInteger duration) {
		this.duration = duration;
	}
	public String getDisposition() {
		return disposition;
	}
	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}
	public BigInteger getAmaflags() {
		return amaflags;
	}
	public void setAmaflags(BigInteger amaflags) {
		this.amaflags = amaflags;
	}
	public String getAccountcode() {
		return accountcode;
	}
	public void setAccountcode(String accountcode) {
		this.accountcode = accountcode;
	}
	public String getUniqueid() {
		return uniqueid;
	}
	public void setUniqueid(String uniqueid) {
		this.uniqueid = uniqueid;
	}
	public String getGravacao() {
		return gravacao;
	}
	public void setGravacao(String gravacao) {
		this.gravacao = gravacao;
	}
	public Cdr() {
		super();
	}
	
}

@Embeddable  
class CdrPK implements Serializable{
	
	private static final long serialVersionUID = -7826522801561247436L;

	@Column(name = "billsec")
	private BigInteger billsec; 
	
	@Column(name = "channel")
	private String channel;

	public BigInteger getBillsec() {
		return billsec;
	}

	public void setBillsec(BigInteger billsec) {
		this.billsec = billsec;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public CdrPK() {
		super();
	}
	
}  
