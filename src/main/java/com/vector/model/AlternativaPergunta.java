package com.vector.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tr_alternativa_pergunta", schema="monitoria")
public class AlternativaPergunta implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="cd_alternativa_pergunta")
	private Long cdAlternativaPergunta;
	
	
	@Column(name="cd_pergunta")
	private Long cdPergunta;
	
	@OneToOne
	@JoinColumn(name="cd_alternativa", referencedColumnName="cd_alternativa")
	private Alternativa alternativa;
	
	@Column(name="alternativa_correta")
	private boolean correta;
	
	@Column(name="condicional")
	private boolean condicional;
	

	public boolean isCorreta() {
		return correta;
	}

	public void setCorreta(boolean correta) {
		this.correta = correta;
	}


	public Long getCdAlternativaPergunta() {
		return cdAlternativaPergunta;
	}

	public void setCdAlternativaPergunta(Long cdAlternativaPergunta) {
		this.cdAlternativaPergunta = cdAlternativaPergunta;
	}

	public Long getCdPergunta() {
		return cdPergunta;
	}

	public void setCdPergunta(Long cdPergunta) {
		this.cdPergunta = cdPergunta;
	}


	public void setAlternativa(Alternativa cdAlternativa) {
		this.alternativa = cdAlternativa;
	}

	
	public boolean isCondicional() {
		return condicional;
	}

	public void setCondicional(boolean condicional) {
		this.condicional = condicional;
	}

	public Alternativa getAlternativa() {
		return alternativa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdAlternativaPergunta == null) ? 0 : cdAlternativaPergunta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlternativaPergunta other = (AlternativaPergunta) obj;
		if (cdAlternativaPergunta == null) {
			if (other.cdAlternativaPergunta != null)
				return false;
		} else if (!cdAlternativaPergunta.equals(other.cdAlternativaPergunta))
			return false;
		return true;
	}

	
	
}
