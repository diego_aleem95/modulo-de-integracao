package com.vector.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_tema", schema="monitoria")
public class Tema implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="cd_tema")
	private Long cdTema;
	
	@Column(name="ds_tema")
	private String dsTema;
	
	@Column(name="status")
	private boolean status;
	
	@Column(name="dt_inativacao")
	private Date dtInativacao;
	
	public Tema(Long cdTema, String dsTema, boolean status, Date date) {
		this.cdTema = cdTema;
		this.dsTema = dsTema;
		this.status = status;
		this.dtInativacao = date;
	}
	
	public Tema() {}

	
	
	public Date getDtInativacao() {
		return dtInativacao;
	}

	public void setDtInativacao(Date dtInativacao) {
		this.dtInativacao = dtInativacao;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	
	public Long getCdTema() {
		return cdTema;
	}

	public void setCdTema(Long cdTema) {
		this.cdTema = cdTema;
	}

	public String getDsTema() {
		return dsTema;
	}

	public void setDsTema(String dsTema) {
		this.dsTema = dsTema;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdTema == null) ? 0 : cdTema.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tema other = (Tema) obj;
		if (cdTema == null) {
			if (other.cdTema != null)
				return false;
		} else if (!cdTema.equals(other.cdTema))
			return false;
		return true;
	}
	
	
	
}
