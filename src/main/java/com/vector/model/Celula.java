package com.vector.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_celula", schema="crm")
public class Celula implements Serializable{
	
	private static final long serialVersionUID = -2571795799797461142L;

	@Id
	@Column(name="cd_celula")
	private int cdCelula;
	
	@Column(name="nm_celula")
	private String nomeCelula;

	public int getCdCelula() {
		return cdCelula;
	}

	public void setCdCelula(int cdCelula) {
		this.cdCelula = cdCelula;
	}

	public String getNomeCelula() {
		return nomeCelula;
	}

	public void setNomeCelula(String nomeCelula) {
		this.nomeCelula = nomeCelula;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cdCelula;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Celula other = (Celula) obj;
		if (cdCelula != other.cdCelula)
			return false;
		return true;
	}

	public Celula(int cdCelula, String nomeCelula) {
		this.cdCelula = cdCelula;
		this.nomeCelula = nomeCelula;
	}
	
	public Celula() {}
	
	
}
