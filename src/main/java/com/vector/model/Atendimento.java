package com.vector.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tb_atendimento", schema="crm")
public class Atendimento {

	@Id
	@Column(name="cdAtendimento")
	private Long cdAtendimento;
	
	@ManyToOne
	@JoinColumn(name="cd_modulo", referencedColumnName="cd_modulo")
	private Modulo modulo;
	
	@ManyToOne
	@JoinColumn(name="cd_celula", referencedColumnName="cd_celula")
	private Celula celula;
	
	@ManyToOne
	@JoinColumn(name="cd_demandante", referencedColumnName="cd_demandante")
	private Demandante demandante;
	
	@ManyToOne
	@JoinColumn(name="cd_usuario", referencedColumnName="cd_usuario")
	private Usuario usuario;
	
	@Column(name="nu_protocolo_atendimento")
	private Long protocoloAtendimento;
	
	@Column(name="dt_abertura")
	private Date dtAbertura;
	
	@Column(name="dt_fechamento")
	private Date dtFechamento;
	
	@Column(name="cd_chamado")
	private Long cdChamado;
	
	@Column(name="cd_presencial")
	private Long cdPresencial;
	
	@Column(name="cd_forma_contato")
	private int cdFormaContato;

	@Column(name="cd_carta")
	private Long cdCarta;
	
	@Column(name="cd_email")
	private Long cdEmail;
	
	@Column(name="cd_fax")
	private Long cdFax;
	
	@Column(name="cd_chat")
	private Long cdChat;
	
	@Column(name="in_manual")
	private Boolean manual;
	
	@Column(name="cd_servico")
	private Long cdServico;
	
	@Column(name="ds_gravacao")
	private String dsGravacao;

	public Boolean getManual() {
		return manual;
	}

	public void setManual(Boolean manual) {
		this.manual = manual;
	}

	public Long getCdServico() {
		return cdServico;
	}

	public void setCdServico(Long cdServico) {
		this.cdServico = cdServico;
	}

	public Long getCdAtendimento() {
		return cdAtendimento;
	}

	public void setCdAtendimento(Long cdAtendimento) {
		this.cdAtendimento = cdAtendimento;
	}

	public Modulo getModulo() {
		return modulo;
	}
	
	public String getDsGravacao() {
		return dsGravacao;
	}

	public void setDsGravacao(String dsGravacao) {
		this.dsGravacao = dsGravacao;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public Celula getCelula() {
		return celula;
	}

	public void setCelula(Celula celula) {
		this.celula = celula;
	}

	

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Long getProtocoloAtendimento() {
		return protocoloAtendimento;
	}

	public void setProtocoloAtendimento(Long protocoloAtendimento) {
		this.protocoloAtendimento = protocoloAtendimento;
	}

	public Date getDtAbertura() {
		return dtAbertura;
	}

	public void setDtAbertura(Date dtAbertura) {
		this.dtAbertura = dtAbertura;
	}

	public Date getDtFechamento() {
		return dtFechamento;
	}

	public void setDtFechamento(Date dtFechamento) {
		this.dtFechamento = dtFechamento;
	}

	public Long getCdChamado() {
		return cdChamado;
	}

	public void setCdChamado(Long cdChamado) {
		this.cdChamado = cdChamado;
	}

	public Long getCdPresencial() {
		return cdPresencial;
	}

	public void setCdPresencial(Long cdPresencial) {
		this.cdPresencial = cdPresencial;
	}

	public int getCdFormaContato() {
		return cdFormaContato;
	}

	public void setCdFormaContato(int cdFormaContato) {
		this.cdFormaContato = cdFormaContato;
	}

	public Long getCdCarta() {
		return cdCarta;
	}

	public void setCdCarta(Long cdCarta) {
		this.cdCarta = cdCarta;
	}

	public Long getCdEmail() {
		return cdEmail;
	}

	public void setCdEmail(Long cdEmail) {
		this.cdEmail = cdEmail;
	}

	public Long getCdFax() {
		return cdFax;
	}

	public void setCdFax(Long cdFax) {
		this.cdFax = cdFax;
	}

	public Long getCdChat() {
		return cdChat;
	}

	public void setCdChat(Long cdChat) {
		this.cdChat = cdChat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdAtendimento == null) ? 0 : cdAtendimento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atendimento other = (Atendimento) obj;
		if (cdAtendimento == null) {
			if (other.cdAtendimento != null)
				return false;
		} else if (!cdAtendimento.equals(other.cdAtendimento))
			return false;
		return true;
	}

	public Atendimento(Long cdAtendimento, Modulo modulo, Celula celula, Demandante demandante, Usuario usuario,
			Long protocoloAtendimento, Date dtAbertura, Date dtFechamento, Long cdChamado, Long cdPresencial,
			int cdFormaContato, Long cdCarta, Long cdEmail, Long cdFax, Long cdChat) {
		super();
		this.cdAtendimento = cdAtendimento;
		this.modulo = modulo;
		this.celula = celula;
		this.demandante = demandante;
		this.usuario = usuario;
		this.protocoloAtendimento = protocoloAtendimento;
		this.dtAbertura = dtAbertura;
		this.dtFechamento = dtFechamento;
		this.cdChamado = cdChamado;
		this.cdPresencial = cdPresencial;
		this.cdFormaContato = cdFormaContato;
		this.cdCarta = cdCarta;
		this.cdEmail = cdEmail;
		this.cdFax = cdFax;
		this.cdChat = cdChat;
	}

	
	public Demandante getDemandante() {
		return demandante;
	}

	public void setDemandante(Demandante cdDemandante) {
		this.demandante = cdDemandante;
	}

	public Atendimento() {}
	


	
}
